<?php

namespace GoCatalyze\SyncCenter\Extensions\GOC;

use GoCatalyze\SyncCenter\Entity\Query\BaseEntityQuery;

class GOCEntityQuery extends BaseEntityQuery
{

    /** @var string */
    protected $entity_query_name = 'goc.entity';

    /** @var string[] Name of query field for entity.updated */
    protected $query_field_updated_at = [
        'user'          => 'created',
        'file'          => 'created',
        'taxonomy_term' => 'tid',
        'role'          => 'rid',
        'default'       => 'changed'
    ];

    /**
     * {@inheritdoc}
     * @param int|DateTime $since
     * @param string $type
     */
    public function queryLatestEntities($since, $type)
    {
        if ($column = $this->getQueryFieldUpdatedAt($type)) {
            $since = is_int($since) ? $since : $since->format(\DATE_ISO8601);
            $this->condition($column, $since, '>');
            $this->sort($column, 'ASC');
        }
    }

    /**
     * Convert query to string.
     *
     *      entity/user.json?
     *          sort_by=changed
     *          &sort_dir=DESC
     *          &condition[changed]=1406087907
     *          &condition[changed][operation]=>
     *          &condition[changed][value]=%timestamp
     *
     * @return string
     */
    public function __toString()
    {
        $params = [];

        // rest_server doesn't support group of condition.
        foreach ($this->condition_groups['default']['expressions'] as $cond) {
            if ('=' === $cond[2]) {
                $params[] = strtr('condition[:key]=:value', [':key' => $cond[0], ':value' => $cond[1]]);
            }
            else {
                $params[] = strtr('condition[:key][operation]=:op&condition[:key]=:value', [
                    ':key'   => $cond[0],
                    ':value' => $cond[1],
                    ':op'    => $cond[2],
                ]);
            }
        }

        // Sort: only support one sort credential
        foreach ($this->sorts as $field => $dir) {
            $params[] = strtr('sort_by=:field&sort_dir=:dir', [':field' => $field, ':dir' => $dir]);
            break;
        }

        $endpoint = "{$this->remote_entity_type}.json";
        if (!is_null($this->endpoint)) {
            $endpoint = $this->endpoint;
        }

        return implode('&', $params);
    }

}
