<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Drupal;

use DateTime;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntityQuery;

/**
 * @group DrupalEntityQuery
 */
class DrupalEntityQueryTest extends DrupalTestCase
{

    public function getQuery()
    {
        return new DrupalEntityQuery();
    }

    public function testInit()
    {
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface', $this->getQuery());
    }

    public function testBuildQueryLatest()
    {
        $query = $this->getQuery();

        $since = strtotime('- 10 days');
        $query->queryLatestEntities($since, 'node');

        $expecting = str_replace('%timestamp', $since, 'node.json?parameters[changed][operation]=>&parameters[changed][value]=%timestamp&sort=changed&direction=ASC');
        $this->assertEquals($expecting, (string) $query);

        $query->setIsViewsResource(true);
        $_expecting = str_replace('%timestamp', $since, 'node.json?changed=%timestamp&sort_by=changed&sort_order=ASC');
        $this->assertEquals($_expecting, (string) $query);
    }

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Do not know sort column for my_custom_entity
     */
    public function testBuildQueryLatestException()
    {
        $query = $this->getQuery();
        $query->queryLatestEntities(new DateTime, 'my_custom_entity');
    }

    public function testQueryCustomOperation()
    {
        $query = $this->getQuery();
        $query->condition('status', 0, '<>');
        $query->condition('type', 'article', '=');
        $expecting = 'node.json?parameters[status][operation]=<>&parameters[status][value]=0&parameters[type]=article';
        $this->assertEquals($expecting, (string) $query);
    }

    public function testQueryCustomRemoteEntityType()
    {
        $query = $this->getQuery();
        $query->setRemoteEntityType('user');
        $query->condition('status', 0, '<>');
        $query->condition('uid', 100, '<=');
        $query->fields(['uid', 'name', 'mail', 'status']);
        $query->setEndpoint('custom_user_endpoint.json');
        $expecting = 'custom_user_endpoint.json';
        $expecting .= '?fields=uid,name,mail,status';
        $expecting .= '&parameters[status][operation]=<>&parameters[status][value]=0';
        $expecting .= '&parameters[uid][operation]=<=&parameters[uid][value]=100';
        $this->assertEquals($expecting, (string) $query);
    }

}
