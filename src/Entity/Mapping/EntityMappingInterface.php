<?php

namespace GoCatalyze\SyncCenter\Entity\Mapping;

interface EntityMappingInterface
{

    /**
     * Add new item to mapping.
     *
     * @param EntityMappingItemInterface $item
     */
    public function addMappingItem(EntityMappingItemInterface $item);

    /**
     * @return EntityMappingItemInterface[]
     */
    public function getMappingItems();

    /**
     * Get mapping item using name of source attribute.
     *
     * @param string $source
     * @return EntityMappingItemInterface
     */
    public function getMappingItem($source);
}
