<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures\Entity;

use GoCatalyze\SyncCenter\Entity\BaseEntity;

class SourceEntity extends BaseEntity
{

    protected $entity_name = 'demo.source';

    /**
     * Attribute info.
     *
     * @var array
     */
    protected $rules = [
        'foo' => [
            'description' => 'Just a foo',
            'required' => true,
            'type' => 'mixed',
        ],
        'source_boolean' => [
            'description' => 'Demo Boolean attribute',
            'type' => 'bool',
        ],
        'source_string' => [
            'description' => 'Demo string attribute',
            'type' => 'string',
        ],
        'source_timestamp' => [
            'description' => 'Date time in timestamp format',
            'type' => 'GoCatalyze\\SyncCenter\\Testing\\Fixtures\\Entity\Attributes\\Timestamp',
        ],
    ];

}
