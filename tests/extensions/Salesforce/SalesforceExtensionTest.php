<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Salesforce;

use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceExtension;

/**
 * @group SalesforceExtension
 */
class SalesforceExtensionTest extends SalesforceTestCase
{

    public function getExtension()
    {
        return new SalesforceExtension();
    }

    public function testInit()
    {
        $extension = $this->getExtension();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\ExtensionInterface', $extension);
        $this->assertEquals('salesforce', $extension->getName());

        foreach ($extension->getEntityTypes() as $type) {
            $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $type);
        }

        foreach ($extension->getEntityQueries() as $qbuilder) {
            $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface', $qbuilder);
        }

        foreach ($extension->getServices() as $service) {
            $this->assertInstanceOf('GoCatalyze\SyncCenter\ServiceInterface', $service);
        }
    }

}
