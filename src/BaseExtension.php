<?php

namespace GoCatalyze\SyncCenter;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\ExtensionInterface;
use GoCatalyze\SyncCenter\ServiceInterface;

/**
 * Abstract extension.
 */
abstract class BaseExtension implements ExtensionInterface
{
    /** @var string */
    protected $extension_name = '';

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @return string
     */
    public function getName()
    {
        return $this->extension_name;
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @return EntityInterface[]
     */
    public function getEntityTypes()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @return ServiceInterface[]
     */
    public function getServices()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @return EntityQueryInterface[]
     */
    public function getEntityQueries()
    {
        return [];
    }

}
