<?php

namespace GoCatalyze\SyncCenter\Entity\Mapping;

use GoCatalyze\SyncCenter\Entity\EntityInterface;

class EntityConvertor extends EntityConvertorBase
{

    /**
     * {@inheritedoc}
     * @param EntityInterface $source_entity
     * @param string|EntityInterface $dest_entity
     * @param EntityMappingInterface $field_mapping
     */
    public function convert(EntityInterface $source_entity, $dest_entity, EntityMappingInterface $field_mapping)
    {
        /* @var $dest_entity EntityInterface */
        $dest_entity = is_string($dest_entity) ? $this->manager->getEntityType($dest_entity) : $dest_entity;
        $this->convertEntityAttributes($source_entity, $dest_entity, $field_mapping);
        $this->detectDestinationEntityId($dest_entity);
        return $dest_entity;
    }

    private function convertEntityAttributes(EntityInterface $source_entity, EntityInterface $dest_entity, EntityMappingInterface $field_mapping)
    {
        $attribute_convertor = $this->getAttributeMappingConvertor();
        foreach ($field_mapping->getMappingItems() as $mapping_item) {
            $attr_value = $attribute_convertor->convertAttribute($source_entity, $mapping_item);
            if (null !== $attr_value) {
                $attribute_convertor->injectEntityAttribute($dest_entity, $mapping_item->getDestination(), $attr_value);
            }
        }
    }

    private function detectDestinationEntityId(EntityInterface $dest_entity)
    {
        $id = $this->getDestinationEntityIdDetector()->detectId($dest_entity, $this->destination_unique_fields);
        if (null !== $id = $this->getDestinationEntityIdDetector()->detectId($dest_entity, $this->destination_unique_fields)) {
            $id_key = $this->destination_service->getRemoteEntityIdKey($this->dest_entity_type);
            $dest_entity->setAttributeValue($id_key, $id);
        }
    }

}
