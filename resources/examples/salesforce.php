<?php

use GoCatalyze\Salesforce\Helper\SalesforceUtil;
use GoCatalyze\Salesforce\SalesforceEntity;
use GoCatalyze\Salesforce\SalesforceExtension;
use GoCatalyze\SyncCenter\BaseManager;

/* @var $manager BaseManager */
$manager = require_once __DIR__ . '/bootstrap.php';
$manager->registerExtension(new SalesforceExtension());
$service = $manager->getService('Salesforce', [
    'client_id' => '3MVG9Y6d_Btp4xp6APU9YunLUkpybUef4Rv0oa0ZY95mtVAtRlN25y8FoumZ6DnRNdE.AkUqxIVtM7d05QUup',
    'client_secret' => '5079437154981735039',
    // 'instance_url' => 'https://ap1.salesforce.com',
    // password access requires the following details
    // 'username' => 'salesforce@univate.com.au',
    // 'password' => 'XFrc3cruRbVsvBILbXLpxLjyUxhjHa3oJlbo', // appead security token to password (vBILbXLpxLjyUxhjHa3oJlbo)
    // or oauth access requires the following details
    // 'refresh_token' => '5Aep8617VFpoP.M.4unohPkPAmgDWw8wUZOjvjadQE41JHJBqYF6maXx_G9BhtayI5zoto97H8jNaf0oZTTbX5K',
    'instance_url' => 'https://test.salesforce.com',
    'username' => 'gm@syn.org.au.go1',
    'password' => '0427smsSMSd5WzXNHBtQCiaub9ZRQGbgAW',
]);

// ---------------------
// Debug, get object info
// ---------------------
/* @var $client SalesforceUtil */
$client = $service->getClient()->getRealClient();
// $info = $client->getObjectTypeInfo('Contact');
$info = $client->getFields('Contact');
print_r($info);

// ---------------------
// Query lastest Accounts
// ---------------------
$query = $manager->getEntityQuery('salesforce.entity');
$query->queryLatestEntities(new DateTime('2014-01-01 00:00:00'), 'Account');
print_r($service->query($query));

// ---------------------
// Query Accounts with custom criterias
// ---------------------
$query = $manager->getEntityQuery('salesforce.entity');
$query->sort('LastModifiedDate', 'ASC');
print_r($entities = $service->query($query, 'Account'));

// ---------------------
// create new Account
// ---------------------
$entity = SalesforceEntity::importFromArray([
        'object_type' => 'Account',
        // See full options at https://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_account.htm
        'AccountNumber' => 12345,
        'Name' => 'Mr Testing',
        'Description' => 'Account object just de testing. Please delete me.',
        'Website' => 'http://gocatalyze.com',
        'YearStarted' => '2014'
    ]);
$response = $service->create($entity);
print_r($response);

// ---------------------
// create new Contact
// ---------------------
$entity = SalesforceEntity::importFromArray([
        'object_type' => 'Contact',
        // See full options at https://www.salesforce.com/us/developer/docs/api/Content/sforce_api_objects_contact.htm
        'Email' => 'mrtesting@mailinator.com',
        'FirstName' => 'Mr',
        'LastName' => 'Testing',
    ]);
$response = $service->create($entity);
print_r($response);
