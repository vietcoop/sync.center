<?php

namespace GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\ReferenceFieldConvertor;

use GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil;
use Guzzle\Http\Client;
use Guzzle\Http\ClientInterface;

class ContactReferenceConvertor implements ReferenceFieldConvertorInterface
{

    /**
     * SF Utility
     *
     * @var SalesforceUtil
     */
    private $util;

    public function __construct(SalesforceUtil $util)
    {
        $this->util = $util;
    }

    public function getUtil()
    {
        return $this->util;
    }

    /**
     * {@inheritdoc}
     */
    public function convert($contact_name, ClientInterface $find_client = null)
    {
        // Search for contact with name
        $find_uri = "services/data/v" . $this->util->getVersion() . "/query?q=";
        $find_uri .= urlencode("SELECT Id FROM Contact WHERE Name='{$contact_name}'");

        // avoid conflict with main request, we create an other client object
        $this->util->refreshToken();
        $find_client = null !== $find_client ? $find_client : new Client();
        $find_client->setBaseUrl($this->util->getBaseUrl());
        $find_request = $find_client->get($find_uri, ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $this->util->getConfig('access_token')]);
        $find_response = $find_client->send($find_request)->json();

        if (0 < $find_response['totalSize']) {
            return $find_response['records'][0]['Id'];
        }

        // @codeCoverageIgnoreStart
        return $this->create($contact_name, $find_client);
        // @codeCoverageIgnoreEnd
    }

    public function create($contact_name, ClientInterface $create_client = null)
    {
        $create_client = null !== $create_client ? $create_client : new Client();

        // No contact available, create new one
        list($first_name, $lasts_name) = explode(' ', $contact_name, 2);
        $create_uri = "services/data/v" . $this->util->getVersion() . "/sobjects/Contact/";
        $body = ['FirstName' => $first_name, 'LastName' => $lasts_name];
        $create_request = $create_client->post(
            $create_uri, ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $this->util->getConfig('access_token')], json_encode($body)
        );

        $create_response = $create_client->send($create_request)->json();
        return $create_response['Id'];
    }

}
