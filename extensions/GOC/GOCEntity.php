<?php

namespace GoCatalyze\SyncCenter\Extensions\GOC;

use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity;

class GOCEntity extends DrupalEntity
{

    /** @var string */
    protected $entity_name = 'goc.entity';

}
