<?php

namespace GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertor;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertor;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertorInterface;

/**
 * To avoid duplication data on destination instance, our mapping provide
 * 'destination unique fields' for user to config.
 *
 * Using this property, the detector will find entity ID from destination service.
 * If found, service handler should 'update' instead of 'create'.
 */
class DestinationEntityIdDetector
{

    /** @var EntityConvertor */
    private $entity_convertor;

    /**
     * @param EntityConvertor $entity_convertor
     */
    public function __construct(EntityConvertorInterface $entity_convertor)
    {
        $this->entity_convertor = $entity_convertor;
    }

    /**
     * Find entity id on destination instance.
     *
     * Two case of u.fields:
     *  1. [field_1, field_2] — legacy mode
     *  2. [  [field_a1, field_a2], [field_b1, field_b2]   ]
     *
     * @param EntityInterface $dest_entity
     * @param array $destination_unique_fields
     * @return int|null
     */
    public function detectId(EntityInterface $dest_entity, array $destination_unique_fields)
    {
        if (empty($destination_unique_fields)) {
            return null;
        }

        if ($is_legacy = !is_array($destination_unique_fields[0])) {
            return $this->doDetectId($dest_entity, $destination_unique_fields);
        }

        foreach ($destination_unique_fields as $_destination_unique_fields) {
            $id = $this->doDetectId($dest_entity, $_destination_unique_fields);
            if (null !== $id) {
                return $id;
            }
        }
    }

    /**
     * Using unique-fields, query destination service for entity.
     *
     * @param EntityInterface $entity
     * @param array $uniques
     * @return int|null
     */
    private function doDetectId(EntityInterface $entity, array $uniques)
    {
        // need: remote entity type, …
        $rtype = $this->entity_convertor->getDestinationRemoteType();
        $service = $this->entity_convertor->getDestinationService();
        $query = $this->entity_convertor->getDestinationEntityQueryBuilder();
        $query->setRemoteEntityType($rtype);

        foreach ($uniques as $unique) {
            foreach ($entity->getAttributes() as $attr_name => $attr_value) {
                if ($unique === $attr_name && is_scalar($attr_value)) {
                    $query->condition($attr_name, $attr_value);
                }
            }
        }

        if ($response = $service->query($query, $rtype)) {
            $response = reset($response);
            if ($response instanceof EntityInterface) {
                return $response;
            }

            // Covnert array to entity object
            $found_type = $this->entity_convertor->getDestinationType();
            $found_entity = $this->entity_convertor->getManager()->getEntityType($found_type);
            $found_entity->setAttributeValues($response);
            return $found_entity;
        }
    }

}
