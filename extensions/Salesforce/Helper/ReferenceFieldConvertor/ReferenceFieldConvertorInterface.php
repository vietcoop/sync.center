<?php

namespace GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\ReferenceFieldConvertor;

use GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil;

interface ReferenceFieldConvertorInterface
{

    /**
     * Constructor
     *
     * @param SalesforceUtil $util
     */
    public function __construct(SalesforceUtil $util);

    /**
     * Convert to object ID.
     *
     * @param string $input
     */
    public function convert($input);
}
