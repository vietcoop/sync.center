<?php

namespace GoCatalyze\SyncCenter\Extensions\Drupal\Helper;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingInterface;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntityQuery;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalService;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 * Load latest data from remote instance.
 */
class EntityFullLoader
{

    /** @var DrupalService */
    protected $service;

    /** @var string */
    protected $entity_class = 'GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity';

    /** @var string */
    protected $query_class = 'GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntityQuery';

    public function __construct(DrupalService $service)
    {
        $this->service = $service;
    }

    /**
     * Get service.
     *
     * @return DrupalService
     */
    public function getService()
    {
        return $this->service;
    }

    public function load(DrupalEntity $entity, $type, EntityMappingInterface $mapping, array $uniques = [])
    {
        $side = empty($uniques) ? 'source' : 'destination';

        if ('source' === $side) {
            return $this->loadMissingFields($entity, $type, $mapping, $side);
        }

        if (!empty($uniques) && !$_entity = $this->loadByIdOrUnique($entity, $type, $uniques)) {
            return false;
        }

        return $this->loadMissingFields($_entity, $type, $mapping, $side);
    }

    /**
     * Load remote entity by ID or unique.
     *
     * @param EntityInterface $entity
     * @param string $type
     * @param array $uniques
     * @return boolean
     * @throws InvalidArgumentException
     */
    private function loadByIdOrUnique(EntityInterface $entity, $type, array $uniques)
    {
        // Load by ID
        if ($id = $this->service->getRemoteEntityId($entity, $type)) {
            return $this->service->load($id, $type);
        }
        // load by unique fields
        else {
            if (empty($uniques)) {
                throw new InvalidArgumentException('Require at least one unique field to check entity existence on remote system.');
            }
            return $this->loadEntityByUniques($entity, $type, $uniques);
        }

        // load failed, entity is not available on remote system
        return false;
    }

    /**
     * Load by unique fields.
     *
     * @param EntityInterface $entity
     * @param string $type
     * @param array $uniques
     * @return DrupalEntity|boolean
     */
    protected function loadEntityByUniques(EntityInterface $entity, $type, array $uniques)
    {
        $query = new $this->query_class;
        $query->setRemoteEntityType($type);
        foreach ($uniques as $unique) {
            foreach ($entity->getAttributes() as $attr_name => $attr_value) {
                if ($unique === $attr_name && is_scalar($attr_value)) {
                    $query->condition($attr_name, $attr_value);
                }
            }
        }

        if ($response = $this->service->query($query, $type)) {
            $response = reset($response);
            if ($response instanceof EntityInterface) {
                return $response;
            }
            $found_entity = new $this->entity_class();
            $found_entity->setAttributeValues($response);
            return $found_entity;
        }

        return false;
    }

    /**
     * Loadd missing fields, the fields can be a referenced fields.
     *
     * @TODO When DrupalClient::doLoad() awares $extra_Fifelds, move logic there.
     * @param DrupalEntity $entity
     * @param string $type
     * @param EntityMappingInterface $mapping
     * @param string $side
     * @return DrupalEntity
     */
    private function loadMissingFields(DrupalEntity $entity, $type, EntityMappingInterface $mapping, $side)
    {
        foreach ($mapping->getMappingItems() as $mapping_item) {
            $field = $side === 'source' ? $mapping_item->getSource() : $mapping_item->getDestination();
            $this->loadMissingField($entity, $type, $field);
        }

        return $entity;
    }

    private function loadMissingField(DrupalEntity $entity, $type, $field)
    {
        if (is_array($field)) {
            foreach ($field as $_field) {
                return $this->loadMissingField($entity, $type, $_field);
            }
        }

        $key = $this->service->getRemoteEntityIdKey($type);
        $id = $this->service->getRemoteEntityId($entity, $type);

        $matches = [];
        if (!preg_match('/^(\w+)\[(\w+)\]\/(\w+)$/', $field, $matches)) { // profile2[main]/field_full_name
            return;
        }

        list($ref_type, $ref_bundle, $ref_field) = [$matches[1], $matches[2], $matches[3]];
        if ($ref_entity = $this->loadRefEntity($ref_type, $key, $id)) {
            $ref_value = $ref_entity->getAttributeValue($ref_field);
            $entity->setAttributeValue($field, $ref_value);
        }
    }

    /**
     * Perform an other query to find referenced entity.
     *
     * @param string $type
     * @param string $source_entity_key
     * @param int $source_entity_id
     * @return bool|DrupalEntity
     */
    private function loadRefEntity($type, $source_entity_key, $source_entity_id)
    {
        static $results = [];

        if (empty($results[$source_entity_key][$source_entity_id][$type])) {
            $results[$source_entity_key][$source_entity_id][$type] = false;

            $query = new DrupalEntityQuery();
            $query->setRemoteEntityType($type);
            $query->condition($source_entity_key, $source_entity_id);

            if ($response = $this->service->query($query, $type)) {
                $entity = new DrupalEntity();
                $entity->setAttributeValues(reset($response));
                $results[$source_entity_key][$source_entity_id][$type] = $entity;
            }
        }

        return $results[$source_entity_key][$source_entity_id][$type];
    }

}
