<?php

namespace GoCatalyze\SyncCenter\Extensions\Drupal;

use GoCatalyze\SyncCenter\BaseExtension;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntityQuery;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalService;

class DrupalExtension extends BaseExtension
{

    protected $extension_name = 'drupal';

    public function getServices()
    {
        return [new DrupalService($this->manager)];
    }

    /**
     * 
     * @return EntityInterface[]
     */
    public function getEntityTypes()
    {
        return [new DrupalEntity()];
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @return EntityQueryBuilderInterface[]
     */
    public function getEntityQueries()
    {
        return [new DrupalEntityQuery()];
    }

}
