<?php

namespace GoCatalyze\SyncCenter\Entity;

interface EntityAttributeValidatorInterface
{
    /**
     * Validate input.
     *
     * @param mixed $input
     */
    public function validate($input);
}
