<?php

namespace GoCatalyze\SyncCenter\Entity\Query;

use DateTime;

/**
 * A service many have multiple entities, each entity may have different
 * interface to query. That's why the query method is not a built-in of
 * ServiceInterface.
 *
 * @link https://github.com/drupal/drupal/blob/8.x/core/lib/Drupal/Core/Entity/Query/QueryInterface.php
 *
 * Query entities:
 *
 *  // Build query
 *  $query = new DemoEntityQuery();
 *  $query->condition('foo', 'bar', '=');
 *
 *  $query->orCondition('group_one', 'foo', 'FOO', '<>);
 *  $query->orCondition('group_one', 'foo', 'BAR', '<>);
 *
 *  $query->andCondition('group_two', 'status', 1);
 *  $query->andCondition('group_two', 'uid', 10, '<=');
 *
 *  // Execute
 *  $entities = $manager->getService('service_name', $options)->query($query);
 */
interface EntityQueryBuilderInterface
{

    /**
     * Each entity should have its own query class. Then this method helps user
     * know which entity type is being supported here.
     *
     * For example, DrupalEntityInterface::getEntityTypeName() should return
     * 'drupal.entity'. The entity type must be already registered with
     * sync_center manager.
     *
     * @return string
     */
    public function getEntityTypeName();

    /**
     * Set remote entity type.
     * 
     * @param string $remote_entity_type
     */
    public function setRemoteEntityType($remote_entity_type);

    /**
     * Get remote entity type
     *
     * @return string
     */
    public function getRemoteEntityType();

    /**
     * Add condition to default group.
     *
     * @param string $field
     * @param mixed $value
     * @param string $op Operation.
     */
    public function condition($field, $value, $op = '');

    /**
     * Add condition to an or-group.
     *
     * @param string $group_name
     * @param string $field
     * @param mixed $value
     * @param string $op
     * @return EntityQueryBuilderInterface
     */
    public function orCondition($group_name, $field, $value, $op = '');

    /**
     * Add a condition to and-group.
     *
     * @param string $group_name
     * @param string $field
     * @param mixed $value
     * @param string $op
     * @return EntityQueryBuilderInterface
     */
    public function andCondition($group_name, $field, $value, $op = '');

    /**
     * Get condition groups.
     *
     * @return array
     */
    public function getConditionGroups();

    /**
     * Add sort.
     *
     * @param $field
     * @param string $direction
     * @return EntityQueryBuilderInterface
     */
    public function sort($field, $direction = 'ASC');

    /**
     * Add select fields.
     *
     * @param array $fields
     * @return EntityQueryBuilderInterface
     */
    public function fields($fields);

    /**
     * Get query fields.
     *
     * @return string[]
     */
    public function getFields();

    /**
     * Query latest entites from remote service, this should just be a wrapper
     * for query() method.
     *
     * @param int|DateTime $since
     * @param string $entity_type
     */
    public function queryLatestEntities($since, $entity_type);
}
