<?php

use GoCatalyze\SyncCenter\BaseManager;

// ---------------------
// Autoload
// ---------------------
$locations[] = __DIR__ . "/../vendor/autoload.php";
$locations[] = __DIR__ . "/../../../autoload.php";

foreach ($locations as $location) {
    if (is_file($location)) {
        $loader = require $location;
        break;
    }
}

return new BaseManager();
