<?php

namespace GoCatalyze\SyncCenter;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\EntityQueryInterface;
use GoCatalyze\SyncCenter\ServiceInterface;

/**
 * Extension is an collection of entity-type, service, client
 */
interface ExtensionInterface
{

    /**
     * Get name of extension. Example: 'drupal'
     *
     * @return string
     */
    public function getName();

    /**
     * Get entity types.
     *
     * @return EntityInterface[]
     */
    public function getEntityTypes();

    /**
     * Get services.
     *
     * @return ServiceInterface[]
     */
    public function getServices();

    /**
     * Get entity queries.
     *
     * @return EntityQueryInterface[]
     */
    public function getEntityQueries();
}
