<?php

namespace GoCatalyze\SyncCenter\Testing\Entity;

use GoCatalyze\SyncCenter\Testing\Fixtures\Entity\SourceEntity;
use GoCatalyze\SyncCenter\Testing\InterfacesTestCase;
use UnexpectedValueException;

class EntitySerializeTest extends InterfacesTestCase
{

    private function getEntityStructure()
    {
        return [
            'foo' => 'FOO',
            'source_boolean' => true,
            'source_string' => 'Hello'
        ];
    }

    public function testSerialize()
    {
        $entity = $this->getManager()->entityFromArray($this->getEntityStructure(), 'demo.source', 'node');

        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $entity);
        $this->assertEquals('FOO', $entity->getAttributeValue('foo'));
        $this->assertEquals(true, $entity->getAttributeValue('source_boolean'));
        $this->assertEquals('Hello', $entity->getAttributeValue('source_string'));
    }

    /**
     * @expectedException UnexpectedValueException
     * @expectedExceptionMessage Unexpected value for entity attribute: source_boolean
     */
    public function testFailSerialize()
    {
        $input = ['source_boolean' => 'Wrong bool'] + $this->getEntityStructure();
        $entity = $this->getManager()->entityFromArray($input, 'demo.source', 'node');
    }

    public function testUnserialize()
    {
        $entity = new SourceEntity();
        $entity->setAttributeValue('foo', 'FOO');
        $entity->setAttributeValue('source_boolean', true);
        $entity->setAttributeValue('source_string', 'Hello');

        $this->assertEquals(
            ['foo' => 'FOO', 'source_boolean' => true, 'source_string' => 'Hello'],
            $this->getManager()->entityToArray($entity, 'node')
        );
    }

}
