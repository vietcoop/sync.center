<?php

namespace GoCatalyze\SyncCenter;

use GoCatalyze\SyncCenter\ClientInterface;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use GoCatalyze\SyncCenter\ServiceInterface;
use RuntimeException;

/**
 * Abstract OAuth service.
 */
abstract class BaseService implements ServiceInterface
{

    /** @var $service_name Name of service */
    protected $service_name;

    /** @var array */
    protected $config = [];

    /** @var ClientInterface */
    protected $client = NULL;

    /** @var array */
    protected $remote_entity_info;

    /** @var array */
    protected $config_schema;

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName()
    {
        return $this->service_name;
    }

    /**
     * {@inheritdoc}
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @codeCoverageIgnore
     */
    protected function getDefaultClient()
    {
        throw new RuntimeException('Unimplemented method: getDefaultClient');
    }

    /**
     * {@inheritdoc}
     * @return ClientInterface
     */
    public function getClient()
    {
        if (null === $this->client) {
            $this->setClient($this->getDefaultClient());
        }

        return $this->client;
    }

    /**
     * {@inheritdoc}
     * @param array $config
     */
    public function setConfiguration(array $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getConfigurationSchema()
    {
        return $this->config_schema;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getConfiguration()
    {
        return $this->config;
    }

    /**
     * {@inheritdoc}
     *
     * Not really needed.
     *
     * @codeCoverageIgnore
     * @param array $error
     * @return boolean
     */
    public function validateService(array &$error)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     * @param EntityQueryBuilderInterface $query
     * @param string $type
     * @return array
     */
    public function query(EntityQueryBuilderInterface $query, $type)
    {
        return $this->getClient()->doQuery($query, $type);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @param string $type
     * @param bool $refresh
     */
    public function getRemoteEntityInfo($type = null, $refresh = false)
    {
        if (null === $this->remote_entity_info) {
            throw new RuntimeException('Unimplemented method: ' . $this->getName() . '::getRemoteEntityInfo.');
        }
        return $this->remote_entity_info;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getRemoteEntityIdKey($type)
    {
        throw new RuntimeException('Unimplemented method: ' . $this->getName() . '::getRemoteEntityId');
    }

    /**
     * {@inheritdoc}
     * @param EntityInterface $entity
     * @param type $type
     * @throws RuntimeException
     */
    public function getRemoteEntityId(EntityInterface $entity, $type)
    {
        if ($key = $this->getRemoteEntityIdKey($type)) {
            return $entity->getAttributeValue($key);
        }
        return null;
    }

    /**
     * {@inheritdoc}
     * @param EntityInterface $entity
     * @param string $remote_entity_type
     */
    public function create(EntityInterface $entity, $remote_entity_type = '')
    {
        return $this->getClient()->doCreate($entity, $remote_entity_type);
    }

    /**
     * {@inheritdoc}
     * @param EntityInterface $entity
     * @param int|string $id
     * @param string $remote_entity_type
     */
    public function update(EntityInterface $entity, $id, $remote_entity_type = '')
    {
        return $this->getClient()->doUpdate($entity, $id, $remote_entity_type);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @param EntityInterface $entity
     * @param string $type
     * @param EntityMappingInterface $mapping
     * @param string[] $uniques
     * @throws \RuntimeException
     */
    public function loadFull(EntityInterface $entity, $type, EntityMappingInterface $mapping, array $uniques = [])
    {
        throw new RuntimeException('Not yet implemented.');
    }

    /**
     * {@inheritdoc}
     * @param EntityInterface $entity
     * @param string $type
     * @param EntityMappingInterface $mapping
     * @param string[] $uniques
     * @return EntityInterface
     * @throws RuntimeException
     */
    public function merge(EntityInterface $entity, $type, EntityMappingInterface $mapping, array $uniques)
    {
        if ($full_entity = $this->loadFull($entity, $type, $mapping, $uniques)) {
            /* @var $full_entity EntityInterface */
            if ($entity_id_key = $this->getRemoteEntityIdKey($type)) {
                if ($entity_id = $full_entity->getAttributeValue($entity_id_key)) {
                    return $this->update($entity, $entity_id, $type);
                }
            }
            else {
                throw new RuntimeException('Can not update remote entity without an entity ID.');
            }
        }
        return $this->create($entity, $type);
    }

    /**
     * {@inheritdoc}
     * @param int|string $id
     */
    public function delete($id)
    {
        return $this->getClient()->doDelete($id);
    }

    /**
     * {@inheritdoc}
     * @param int|string $id
     * @param string $remote_entity_type
     * @param string[] $extra_fields
     * @return EntityInterface
     */
    public function load($id, $remote_entity_type, array $extra_fields = [])
    {
        return $this->getClient()->doLoad($id, $remote_entity_type, $extra_fields);
    }

}
