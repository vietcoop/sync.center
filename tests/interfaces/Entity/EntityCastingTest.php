<?php

namespace GoCatalyze\SyncCenter\Testing\Entity;

use DateTime as DateTime2;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMapping;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingItem;
use GoCatalyze\SyncCenter\ManagerInterface;
use GoCatalyze\SyncCenter\Testing\Fixtures\DemoManager;
use GoCatalyze\SyncCenter\Testing\Fixtures\Entity\DestEntity;
use GoCatalyze\SyncCenter\Testing\Fixtures\Entity\SourceEntity;
use GoCatalyze\SyncCenter\Testing\InterfacesTestCase;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Exception\OutOfBoundsException;
use Symfony\Component\Validator\Exception\RuntimeException;
use UnexpectedValueException;

/**
 * @group entityCasting
 */
class EntityCastingTest extends InterfacesTestCase
{

    /**
     * @param ManagerInterface $manager
     * @return EntityInterface
     */
    private function getSourceEntity()
    {
        return $this->getManager()->entityFromArray(['foo' => 'FOO'], 'demo.source', 'node');
    }

    private function getMapping()
    {
        $mapping = new EntityMapping();
        $mapping->addMappingItem(new EntityMappingItem('foo', 'bar'));
        $mapping->addMappingItem(new EntityMappingItem('source_boolean', 'dest_string'));
        $mapping->addMappingItem(new EntityMappingItem('source_timestamp', 'dest_datetime', function($timestamp) {
            $datetime = new DateTime2();
            $datetime->setTimestamp($timestamp);
            return $datetime;
        }));
        return $mapping;
    }

    public function testHappy()
    {
        $manager = new DemoManager();
        $source_entity = $this->getSourceEntity($manager);
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $source_entity);

        $dest_entity = $manager
            ->getEntityConvertor()
            ->convert($source_entity, 'demo.destination', $this->getMapping())
        ;

        /* @var $dest_entity  EntityInterface */
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $dest_entity);
        $this->assertEquals('FOO', $dest_entity->getAttributeValue('bar'));
    }

    public function testEntityMapping()
    {
        $array[] = ['source' => 'nid', 'destination' => 'ID'];
        $entity_mapping = EntityMapping::fromArray($array);

        $item = $entity_mapping->getMappingItems()['nid'];

        $this->assertEquals(['nid', 'ID'], [$item->getSource(), $item->getDestination()]);
    }

    /**
     * Destination entity.dest_string is expecting 'string', boolean given.
     *
     * @expectedException UnexpectedValueException
     * @expectedExceptionMessage Unexpected value for entity attribute: source_boolean
     */
    public function testWrongStringAttribute()
    {
        $source_entity = $this->getManager()->entityFromArray(['foo' => 'FOO', 'source_boolean' => 'Is TRUE!'], 'demo.source', 'node');

        // convert to destination
        (new DemoManager())
            ->getEntityConvertor()
            ->convert($source_entity, 'demo.destination', $this->getMapping())
        ;
    }

    public function testCustomAttributeType()
    {
        $timestamp = time();
        $manager = new DemoManager();
        $source_entity = $this->getSourceEntity($manager);
        $source_entity->setAttributeValue('source_timestamp', $timestamp);

        /* @var $dest_entity DestEntity */
        $dest_entity = $manager
            ->getEntityConvertor()
            ->convert($source_entity, 'demo.destination', $this->getMapping())
        ;

        /* @var $dest_datetime DateTime */
        $dest_datetime = $dest_entity->getAttributeValue('dest_datetime');
        $this->assertInstanceOf('DateTime', $dest_datetime);
        $this->assertEquals($timestamp, $dest_datetime->getTimestamp());
    }

    /**
     * @expectedException OutOfBoundsException
     * @expectedExceptionMessage Content type is not registered: wrong_entity_type
     */
    public function testConvertWrongDestEntityType()
    {
        $source_entity = $this->getManager()->entityFromArray(['nid' => 123, 'title' => 'Node title'], 'demo.source', 'node');

        (new DemoManager())
            ->getEntityConvertor()
            ->convert($source_entity, 'wrong_entity_type', $this->getMapping())
        ;
    }

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Unexpected value for entity attribute: dest_string
     */
    public function testConvertWithError()
    {
        $source_entity = $this->getManager()->entityFromArray(['nid' => 123, 'title' => 'Node title'], 'demo.source', 'node');

        $mapping = EntityMapping::fromArray([
                ['source' => 'nid', 'destination' => 'dest_string'], // inject int to string field
                ['source' => 'title', 'destination' => 'title']
        ]);

        (new DemoManager())
            ->getEntityConvertor()
            ->convert($source_entity, 'demo.destination', $mapping)
        ;
    }

}
