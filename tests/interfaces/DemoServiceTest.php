<?php

namespace GoCatalyze\SyncCenter\Testing;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingInterface;
use GoCatalyze\SyncCenter\Testing\Fixtures\Entity\SourceEntity;
use GoCatalyze\SyncCenter\Testing\Fixtures\Service\DemoService;

/**
 * @group demoService
 */
class DemoServiceTest extends InterfacesTestCase
{

    private function getService()
    {
        return new DemoService();
    }

    public function testServiceName()
    {
        $this->assertEquals('demo.service', $this->getService()->getName());
    }

    public function testServiceConfiguration()
    {
        $service = $this->getService();
        $service->setConfiguration(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $service->getConfiguration());
    }

    public function testInjectClient()
    {
        $service = $this->getService();
        $service->setClient($client = $this->getMock('GoCatalyze\\SyncCenter\\BaseClient'));

        $expected = 'GoCatalyze\\SyncCenter\\ClientInterface';
        $actual = $service->getClient();
        $msg = 'Can inject and get client.';
        $this->assertInstanceOf($expected, $actual, $msg);
    }

    public function testGetDefaultClient()
    {
        $expected = 'GoCatalyze\\SyncCenter\\ClientInterface';
        $actual = $this->getService()->getClient();
        $msg = 'If no client injected, return default client.';
        $this->assertInstanceOf($expected, $actual, $msg);
    }

    public function testGetRemoteEntityId()
    {
        // Case of valid ID
        $entity_1 = $this->getManager()->entityFromArray(['nid' => 111, 'title' => 'Node #111'], 'demo.source', 'node');

        $service_1 = $this->getService();
        $service_1->injectReturn('getRemoteEntityIdKey', 'nid');
        $this->assertEquals(111, $service_1->getRemoteEntityId($entity_1, 'node'));

        // Case of no key
        $entity_2 = $this->getManager()->entityFromArray(['nid' => 111, 'title' => 'Node #111'], 'demo.source', 'node');
        $service_2 = $this->getService();
        $service_2->injectReturn('getRemoteEntityIdKey', null);
        $this->assertEquals(null, $service_2->getRemoteEntityId($entity_2, 'node'));
    }

    /**
     * @dataProvider sourceActions
     */
    public function testActions($action, $arguments)
    {
        // Mock & inject client
        $client = $this->getMock('GoCatalyze\\SyncCenter\\BaseClient');
        $mock = $client->expects($this->once());
        $mock->method('do' . ucfirst($action));
        foreach ($arguments as $v) {
            $with[] = $this->equalTo($v);
        }
        call_user_func_array([$mock, 'with'], $with);
        $mock->willReturn(new SourceEntity());

        $service = $this->getService();
        $service->setClient($client);

        // Action
        $expected = 'GoCatalyze\\SyncCenter\\Entity\\EntityInterface';
        $actual = call_user_func_array([$service, $action], $arguments);
        $this->assertInstanceOf($expected, $actual);
    }

    public function sourceActions()
    {
        $demo_entity = $this->getManager()->entityFromArray(['title' => 'Test title'], 'demo.destination', 'node');
        $query = $this->getMock('GoCatalyze\SyncCenter\Entity\Query\BaseEntityQuery');

        return [
            ['create', [$demo_entity, 'node']],
            ['update', [$demo_entity, 1, 'node']],
            ['load', [1, 'user', ['profile2[main]/field_full_name']]],
            ['delete', [1]],
            ['query', [$query, 'node']]
        ];
    }

    /**
     * @dataProvider sourceMergeAction
     */
    public function testMergeAction($expecting_action, EntityInterface $entity, EntityMappingInterface $mapping)
    {
        $service = $this->getService();

        // Mock service
        $service->injectReturn('loadFull', $entity);
        $service->injectReturn('getRemoteEntityIdKey', 'nid');
        $this->assertEquals($entity->getAttributeValue('nid'), $service->getRemoteEntityId($entity, 'node'));

        // Mock & inject client
        $client = $this->getMock('GoCatalyze\\SyncCenter\\BaseClient');

        $client_mock = $client->expects($this->once());
        $client_mock->method('do' . ucfirst($expecting_action));

        $expecting_action === 'update' ? $client_mock->with($this->equalTo($entity), $this->equalTo(1), $this->equalTo('node')) : $client_mock->with($this->equalTo($entity), $this->equalTo('node'));

        $service->setClient($client);

        // Action:
        //  Execute merge() action on existing entity, client::doUpdate() should
        //  be executed with correct parameters (entity, id, type)
        $service->merge($entity, 'node', $mapping, $unique_fields = []);
    }

    public function sourceMergeAction()
    {
        $entity_update = $this->getManager()->entityFromArray(['nid' => 1, 'title' => 'Node #1'], 'demo.destination', 'node');
        $entity_create = $this->getManager()->entityFromArray(['title' => 'New node'], 'demo.destination', 'node');
        $mapping = \GoCatalyze\SyncCenter\Entity\Mapping\EntityMapping::fromArray([
            ['source' => 'drupal_id__c', 'destination' => 'nid'],
            ['source' => 'slug__c', 'destination' => 'path']
        ]);
        return [
            ['update', $entity_update, $mapping],
            # ['create', $entity_create]
        ];
    }

}
