<?php

namespace GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertor;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertor;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertorInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingItemInterface;

class AttributeMappingConvertor
{

    /** @var EntityConvertor */
    private $entity_convertor;

    public function __construct(EntityConvertorInterface $entity_convertor)
    {
        $this->entity_convertor = $entity_convertor;
    }

    /**
     * Get destination value from source value + mapping config.
     *
     * @param EntityInterface $source_entity
     * @param EntityMappingItemInterface $mapping_item
     * @return mixed
     */
    public function convertAttribute(EntityInterface $source_entity, EntityMappingItemInterface $mapping_item)
    {
        $return = null;
        $source_key = $mapping_item->getSource();

        // source > dest: field_last_name => LastName
        if (is_string($source_key) && isset($source_entity[$source_key])) {
            $return = $source_entity[$source_key];
        }
        elseif (is_array($source_key)) {
            $is_ref = is_array($source_key[0]) ? '@' === substr($source_key[0][0], 0, 1) : '@' === substr($source_key[0], 0, 1);

            // source > dest: [@reference_field<ReferenceType>, [source, fields], [dest, fields]] => dest
            if ($is_ref) {
                $return = $this->convertAttributeWithReferenceObject($source_entity, $mapping_item);
            }
            // complex source > dest: [[field, field], field] => Dest_Field
            else {
                $return = $this->convertAttributeWithArraySource($source_entity, $source_key);
            }
        }

        // mapping item has custom callback to convert value
        if ($callback = $mapping_item->getCallback()) {
            $return = $this->applyCallback($callback, $return);
        }

        return $return;
    }

    /**
     * Convert reference ID from source to destination.
     *
     * Mapping item: [@reference_field<ReferenceType>, [source, fields], [dest, fields]] => dest<ReferenceType>
     * @return Id.
     */
    private function convertAttributeWithReferenceObject(EntityInterface $source_entity, EntityMappingItemInterface $mapping_item)
    {
        // Details of mapping_item.source
        $source = $mapping_item->getSource();
        preg_match('/^@(.+)<(.+)>$/', is_array($source[0]) ? $source[0][0] : $source[0], $s_matches);
        $s_ref_key = $s_matches[1];
        $s_ref_type = $s_matches[2];
        $s_ref_id = $source_entity[$s_ref_key];
        $s_ref_ufields = is_array($source[1]) ? $source[1] : [$source[1]];
        $s_ref_object = $this->entity_convertor->getSourceService()->load($s_ref_id, $s_ref_type);

        // Details of mapping_item.destination
        $dest = $mapping_item->getDestination();
        preg_match('/^@(.+)<(.+)>$/', $dest, $d_matches);
        $d_type = $this->entity_convertor->getDestinationType();
        $d_ref_key = $d_matches[1];
        $d_ref_type = $d_matches[2];
        $d_ref_ufields = is_array($source[2]) ? $source[2] : [$source[2]];
        $d_service = $this->entity_convertor->getDestinationService();

        // alter destnation from `@field_name<RefType>` to `field_name`
        $mapping_item->setDestination($d_ref_key);

        // Query destination to find ref-object
        $d_query = $this->entity_convertor->getManager()->getEntityQuery($d_type);
        for ($i = 0; $i < count($s_ref_ufields); $i++) {
            if (!empty($s_ref_object[$s_ref_ufields[$i]])) {
                $d_query->condition($d_ref_ufields[$i], $s_ref_object[$s_ref_ufields[$i]]);
            }
        }

        if (!empty($d_query->getConditionGroups()['default']['expressions'])) {
            if ($d_ref_objects = $d_service->query($d_query, $d_ref_type)) {
                $d_ref_object = reset($d_ref_objects);
                $id_key = $d_service->getRemoteEntityIdKey($d_ref_type);
                return $d_ref_object[$id_key];
            }
        }
    }

    /**
     * In some case we need field mapping like this:
     *
     * [
     *      [FirstName, LastName], // If found FirstName or LastName, F+L to build value
     *      Name, // FirstName/LastName is empty, use Name instead
     *      Mail // No name available, use value of Mail field for mapping
     * ] => Name
     *
     * Source:
     *   Case 1: [mail] => dest
     *   Case 2: [last_name, first_name] => dest
     *
     * @param EntityInterface $source_entity
     * @param array $source_keys
     * @return mixed
     */
    private function convertAttributeWithArraySource(EntityInterface $source_entity, array $source_keys)
    {
        foreach ($source_keys as $_source_keys) {
            // case of option 1: mail
            if (is_string($_source_keys) && isset($source_entity[$_source_keys])) {
                return $source_entity[$_source_keys];
            }

            // case of option 2 & 3: […, …, …]
            if (is_array($_source_keys)) {
                $return = '';
                foreach ($_source_keys as $attr_name) {
                    if (isset($source_entity[$attr_name])) {
                        $return = trim($return . ' ' . $source_entity[$attr_name]);
                    }
                }

                if (!empty($return)) {
                    return $return;
                }
            }
        }
    }

    private function applyCallback($callback, $return)
    {
        if (is_array($callback) && isset($callback['function']) && isset($callback['arguments'])) {
            // field mapping item:
            //  [DateTimeField, TimestampField, ['function' => 'strtotime', 'arguments' => ['@self']]]
            // @self will be replaced with value of source field — DateTimeField.
            foreach ($callback['arguments'] as &$arg) {
                $arg = '@self' === $arg ? $return : $arg;
            }
            return call_user_func_array($callback['function'], $callback['arguments']);
        }

        return call_user_func_array($callback, [$return]);
    }

    /**
     * Inject a value to entity's attribute.
     *
     * @param EntityInterface $entity
     * @param string $attr_name
     * @param mixed $attr_value
     */
    public function injectEntityAttribute(EntityInterface $entity, $attr_name, $attr_value)
    {
        // not case of address.country
        if (false === strpos($attr_name, '.')) {
            $entity[$attr_name] = $attr_value;
        }
        // case of profile2[default]/field_billing_address.country
        elseif ((false !== strpos($attr_name, '[')) && (false !== strpos($attr_name, ']'))) {
            $entity[$attr_name] = $attr_value;
        }
        else {
            // inject value to property of field->element
            // for example: address.country, address.city, …
            list($attr_name, $pty_name) = explode('.', $attr_name, 2);

            $array = isset($entity[$attr_name]) ? $entity[$attr_name] : [];
            $array[$pty_name] = $attr_value;
            $entity[$attr_name] = $array;
        }
    }

}
