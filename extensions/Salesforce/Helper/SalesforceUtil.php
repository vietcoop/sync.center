<?php

namespace GoCatalyze\SyncCenter\Extensions\Salesforce\Helper;

use GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\ReferenceFieldConvertor\ContactReferenceConvertor;
use GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\ReferenceFieldConvertor\ReferenceFieldConvertorInterface;
use Guzzle\Http\ClientInterface;
use Guzzle\Http\Message\Response;
# use Guzzle\Service\Client;
use Guzzle\Http\Client;
use RuntimeException;

/**
 * Class SalesforceClient
 *
 * @package GoCatalyze\Client
 */
class SalesforceUtil extends Client
{

    private $version = '30.0';

    /** @var ReferenceFieldConvertorInterface[] */
    private $reference_field_convertors = [];

    /** @var array */
    private $entity_info;

    public function __construct($baseUrl = '', $config = null)
    {
        if (isset($config['entity_info'])) {
            $this->entity_info = $config['entity_info'];
            unset($config['entity_info']);
        }

        parent::__construct($baseUrl, $config);
    }

    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return array|mixed
     */
    public function refreshToken(ClientInterface $auth_client = null, $refresh = false)
    {
        static $ran = false;

        if ($ran && !$refresh) {
            return;
        }

        $ran = true;

        // @codeCoverageIgnoreStart
        if (null === $auth_client) {
            if (FALSE !== strpos($this->getConfig('instance_url'), 'test.salesforce.com')) {
                $auth_client = new Client('https://test.salesforce.com');
            }
            else {
                $auth_client = new Client('https://login.salesforce.com');
            }
        }
        // @codeCoverageIgnoreEnd

        $query = [
            'grant_type'    => 'password',
            'client_id'     => $this->getConfig('client_id'),
            'client_secret' => $this->getConfig('client_secret'),
        ];

        $refresh_token = $this->getConfig('refresh_token');
        if (!empty($refresh_token)) {
            $query['grant_type'] = 'refresh_token';
            $query['refresh_token'] = $refresh_token;
        }
        else {
            $query['username'] = $this->getConfig('username');
            $query['password'] = $this->getConfig('password');
        }

        $config = $auth_client->post('services/oauth2/token', [], $query)->send()->json();
        if (!empty($config['instance_url'])) {
            $this->setBaseUrl($config['instance_url']);
        }

        $this->setConfig($config);
    }

    /**
     * @codeCoverageIgnore No logic
     * @return array|mixed
     */
    public function getOrganisationDetails()
    {
        return $this->doQuery("services/data/")->json();
    }

    /**
     * @codeCoverageIgnore No logic
     * @return array|mixed
     */
    public function getOrganisationLimits()
    {
        return $this->doQuery("services/data/v{$this->version}/limits/")->json();
    }

    /**
     * @codeCoverageIgnore No logic
     * @return array|mixed
     */
    public function getOrganisationResources()
    {
        return $this->doQuery("services/data/v{$this->version}/")->json();
    }

    /**
     * @return array
     */
    public function getObjectTypes()
    {
        $sobjects = $this->doQuery("services/data/v{$this->version}/sobjects/");

        $types = [];
        foreach ($sobjects['sobjects'] as $i => $sobject) {
            // results are more than 200, it's a lot!
            if ($sobject['createable'] && $sobject['queryable'] && $sobject['searchable'] && $sobject['triggerable'] && !$sobject['deprecatedAndHidden'] && $sobject['updateable'] && $sobject['layoutable']) {
                $types[] = $sobject['name'];
            }
            else {
                unset($sobjects['sobjects'][$i]);
            }
        }

        return $types;
    }

    /**
     * Get object type information.
     *
     * @staticvar array $info
     * @param string $type
     * @return array
     */
    public function getObjectTypeInfo($type)
    {
        if (empty($this->entity_info[$type]['fields'])) {
            $this->entity_info[$type] = $this->doQuery("services/data/v{$this->version}/sobjects/{$type}/describe/");
            if (!empty($this->entity_info[$type]['fields'])) {
                foreach ($this->entity_info[$type]['fields'] as $i => $field_info) {
                    $this->entity_info[$type]['fields'][$field_info['name']] = $field_info;
                    unset($this->entity_info[$type]['fields'][$i]);
                }
            }
        }

        return $this->entity_info[$type];
    }

    /**
     * Get field names of a object type.
     *
     * @param string $type
     * @return array
     */
    public function getFields($type)
    {
        $fields = [];

        if ($describe = $this->getObjectTypeInfo($type)) {
            // always failed if we query with OtherAddress, MailingAddress fields
            unset($describe['fields']['OtherAddress']);
            unset($describe['fields']['MailingAddress']);

            $fields = array_keys($describe['fields']);
        }

        return $fields;
    }

    /**
     *
     * @param type $ref_object_type
     */
    public function getReferenceFieldConvertor($ref_object_type)
    {
        if (empty($this->reference_field_convertors[$ref_object_type])) {
            switch ($ref_object_type) {
                case 'Contact':
                    $this->reference_field_convertors[$ref_object_type] = new ContactReferenceConvertor($this);
                    break;
                default:
                    throw new RuntimeException(sprintf('Not support converting reference field of %s', $ref_object_type));
                    break;
            }
        }

        return $this->reference_field_convertors[$ref_object_type];
    }

    /**
     * @codeCoverageIgnore No logic
     */
    public function __call($method, $args)
    {
        // Disable magic method in this class.
        // This magic method conflict with query().
    }

    /**
     * {@inheritdoc}
     */
    public function createRequest($method = 'GET', $uri = null, $headers = null, $body = null, array $options = array())
    {
        $headers = null === $headers ? [] : $headers;
        $headers += ['Content-Type' => 'application/json'];

        if (($uri !== 'services/oauth2/token') && !isset($headers['Authorization'])) {
            if (!$this->getConfig('access_token')) {
                $this->refreshToken();
            }
            $headers['Authorization'] = 'Bearer ' . $this->getConfig('access_token');
        }
        return parent::createRequest($method, $uri, $headers, $body, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function doCreate($type, $data)
    {
        // Support simple object reference
        $object_info = $this->getObjectTypeInfo($type);

        foreach ($data as $field_name => &$v) {
            if (!empty($object_info['fields'][$field_name]['referenceTo'][0])) {
                try {
                    $ref_to_type = $object_info['fields'][$field_name]['referenceTo'][0];
                    $convertor = $this->getReferenceFieldConvertor($ref_to_type);
                    $v = $convertor->convert($v);
                }
                catch (RuntimeException $e) {
                    // no convertor available, do nothing.
                }
            }
        }

        $path = "services/data/v{$this->version}/sobjects/{$type}/";
        return $this->createRequest('POST', $path, [], json_encode($data))->send()->json();
    }

    public function doUpdate($type, $id, $data)
    {
        $path = "services/data/v" . $this->version . "/sobjects/{$type}/{$id}";
        return $this->createRequest('PATCH', $path, [], json_encode($data))
                ->send()
                ->json();
    }

    /**
     * @param string $url
     * @return Response
     */
    public function doQuery($url)
    {
        return $this->createRequest('GET', $url)->send()->json();
    }

}
