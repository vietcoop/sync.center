<?php

namespace GoCatalyze\SyncCenter\Extensions\Salesforce;

use GoCatalyze\SyncCenter\BaseClient;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil;
use RuntimeException;

class SalesforceClient extends BaseClient
{

    /** @var SalesforceUtil */
    protected $real_client;

    /**
     * Get real client. Public because we need know more info about salesforce
     * object when debugging.
     *
     * @return SalesforceUtil
     */
    public function getRealClient()
    {
        if (null === $this->real_client) {
            $config = $this->getConfig();
            $this->real_client = new SalesforceUtil($config['instance_url'], $config);
        }
        return $this->real_client;
    }

    public function setRealClient(SalesforceUtil $real_client)
    {
        $this->real_client = $real_client;
    }

    /**
     * {@inheritedoc}
     * @param SalesforceEntityQuery $query
     * @param string $type
     * @return EntityInterface[]
     */
    public function doQuery(EntityQueryBuilderInterface $query, $type)
    {
        $client = $this->getRealClient();
        $query->setRemoteEntityType($type);

        // select fields, salesforce does not support SELECT *?
        if (!$query->getFields()) {
            $query->fields($client->getFields($query->getRemoteEntityType()));
        }

        $path = "services/data/v{$client->getVersion()}/query?q=" . (string) $query;
        $records = $client->doQuery($path, $type);

        if (!empty($records['records'])) {
            // convert results to system entity
            foreach ($records['records'] as $row) {
                $rows[] = $row;
            }
        }

        return !empty($rows) ? $rows : [];
    }

    /**
     * {@inheritedoc}
     * @param EntityInterface $entity
     * @param string $type
     * @return array
     */
    public function doCreate(EntityInterface $entity, $type)
    {
        $required_errors = [];

        foreach ($this->getRealClient()->getObjectTypeInfo($type)['fields'] as $field_name => $field_info) {
            if (!isset($entity[$field_name])) {
                if (!$field_info['nillable'] && !$field_info['defaultedOnCreate']) {
                    $required_errors[] = $field_name;
                }
            }
            // Salesforce does not allow to create object with any value
            else {
                if (!$field_info['createable']) {
                    unset($entity[$field_name]);
                }
                elseif ('boolean' === $field_info['type']) {
                    $entity[$field_name] = (boolean) ($entity[$field_name]);
                }
            }
        }

        if (!empty($required_errors)) {
            $msg = strtr(
                '%type  has requried field(s) but they are not provided: %errors. Input: %input', [
                '%type'   => $type,
                '%errors' => implode(', ', $required_errors),
                '%input'  => json_encode($entity->getAttributes()),
            ]);
            throw new RuntimeException($msg);
        }

        return $this->getRealClient()->doCreate($type, $entity->getAttributes());
    }

    public function doUpdate(EntityInterface $entity, $id, $type)
    {
        foreach ($this->getRealClient()->getObjectTypeInfo($type)['fields'] as $field_name => $field_info) {
            if (isset($entity[$field_name])) {
                if (!$field_info['updateable']) {
                    unset($entity[$field_name]);
                }

                if ('boolean' === $field_info['type']) {
                    $entity[$field_name] = (boolean) ($entity[$field_name]);
                }
            }
        }
        return $this->getRealClient()->doUpdate($type, $id, $entity->getAttributes());
    }

    public function doLoad($id, $type, array $extra_fields = array())
    {
        $client = $this->getRealClient();
        $path = "services/data/v" . $client->getVersion() . "/sobjects/{$type}/{$id}";
        return $client->createRequest('GET', $path)->send()->json();
    }

}
