<?php

namespace GoCatalyze\SyncCenter;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;

/**
 * Service is group of business logic for GoSync to talk with a specific
 * service, like: Twitter, Saleforce, Mailchimp, Drupal, …
 */
interface ServiceInterface
{

    /**
     * Get name of service.
     *
     * @return string
     */
    public function getName();

    /**
     * Get all entity info.
     *
     * @param bool $refresh
     * @return array
     */
    public function getAllRemoteEntityInfo($refresh = false);

    /**
     * In many cases, we want to know the structure of remote entity type:
     *
     * - Salesforce: Is a field a reference field
     * - Drupal entity: Name of ID Key (nid, uid, pid, cid, …)
     *
     * @param string $type
     * @param bool $refresh
     */
    public function getRemoteEntityInfo($type, $refresh = false);

    /**
     * Get ID value of a remote entity.
     *
     * @param EntityInterface $entity
     * @param string $type
     */
    public function getRemoteEntityId(EntityInterface $entity, $type);

    /**
     * Return ID key.
     *
     * @param string $type
     */
    public function getRemoteEntityIdKey($type);

    /**
     * Get configuration schema.
     *
     * @return array
     */
    public function getConfigurationSchema();

    /**
     * Authentication and related info which are needed to perform requests to
     * remote serivce. This is needed when we perform same action on multiple
     * services:
     *
     *  $drupal_1 = $manager->getService('drupal');
     *  $drupal_1->setConfiguration($conf_d1);
     *  $drupal_1->doX();
     *
     *  $drupal_2 = $manager->getService('drupal');
     *  $drupal_2->setConfiguration($conf_d2);
     *  $drupal_2->doX();
     *
     * @param array $configuration
     */
    public function setConfiguration(array $configuration);

    /**
     * @return array Configuration of service.
     */
    public function getConfiguration();

    /**
     * Query for remote entities.
     *
     * @param EntityQueryBuilderInterface $query
     * @param string $type
     */
    public function query(EntityQueryBuilderInterface $query, $type);

    /**
     * $entity object is missing data, only some key value is ready, we wnant
     * fully load it.
     *
     * Remote entity ID can be even empty before this method, $uniques help method
     * implementations to find entity ID from remote service instance.
     *
     * @param EntityInterface $entity
     * @param string $type
     * @param EntityMappingInterface $mapping
     * @param string[] $uniques
     * @return EntityInterface
     */
    public function loadFull(EntityInterface $entity, $type, EntityMappingInterface $mapping, array $uniques = []);

    /**
     * Load remote entity by ID.
     *
     * @param int|string $id
     * @param string $remote_entity_type
     * @param string[] $extra_fields
     */
    public function load($id, $remote_entity_type, array $extra_fields = []);

    /**
     * Create new entity on remote service.
     *
     * @param EntityInterface $entity
     * @param string $remote_entity_type
     */
    public function create(EntityInterface $entity, $remote_entity_type = '');

    /**
     * Update existing entity on remote service.
     *
     * @param EntityInterface $entity
     * @param int|string $id
     * @param string $remote_entity_type
     */
    public function update(EntityInterface $entity, $id, $remote_entity_type = '');

    /**
     * Create or Update existing entity on remote service.
     *
     * If an entity is not existing on remote system, create it.
     * Elese update it.
     * We need $uniques array to check the existence.
     *
     * Original entity is a missing, incomplete object, we need mapping to load
     * all needed data before merge (update/create)
     *
     * @param EntityInterface $entity
     * @param string $type Remote entity type
     * @param EntityMappingInterface $mapping
     * @param string[] $uniques
     */
    public function merge(EntityInterface $entity, $type, EntityMappingInterface $mapping, array $uniques);

    /**
     * Remove an entity from remote service.
     *
     * @param int|string $id
     */
    public function delete($id);

    /**
     * Make sure remote service is working fine.
     *
     * @todo Use Symfony\Validator component.
     * @param array $error
     */
    public function validateService(array &$error);

    /**
     * Inject client object.
     *
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client);

    /**
     * Get client object.
     *
     * @return ClientInterface
     */
    public function getClient();
}
