<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures\EntityQuery;

use GoCatalyze\SyncCenter\Entity\Query\BaseEntityQuery;

class DestEntityQuery extends BaseEntityQuery
{

    public function getEntityTypeName()
    {
        return 'demo.dest';
    }

}
