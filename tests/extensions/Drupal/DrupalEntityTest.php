<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Drupal;

use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity;

/**
 * @group DrupalEntity
 */
class DrupalEntityTest extends DrupalTestCase
{

    public function getEntity()
    {
        return new DrupalEntity();
    }

    /**
     * Create entity directly.
     */
    public function testEntityInit()
    {
        $entity = $this->getEntity();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $entity);
        $this->assertEquals('drupal.entity', $entity->getName());
    }

    /**
     * Create entity from array
     */
    public function testInitFromArray()
    {
        $entity_input = ['nid' => 123, 'title' => 'Node #123'];
        $entity = $this->getManager()->entityFromArray($entity_input, 'drupal.entity', 'node');
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $entity);
        $this->assertEquals('drupal.entity', $entity->getName());
    }

    public function testSetAttrWithLanguageAwareField()
    {
        $entity_input = ['nid' => 123, 'title' => 'Node #123', 'body' => ['und' => [0 => ['value' => 'Body of node #123…']]]];
        $entity = $this->getManager()->entityFromArray($entity_input, 'drupal.entity', 'node');
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $entity);
        $this->assertEquals('drupal.entity', $entity->getName());

        // Can get it correctly
        $this->assertEquals($entity_input['body']['und'][0]['value'], $entity['body']);
    }

    public function testSetReferencedEntity()
    {
        $entity_input = [
            'nid' => 123,
            'title' => 'Node #123',
            'user[default]/name' => 'MrTesting',
            'user[default]/mail' => 'mrtesting@mailinator.com',
        ];

        $entity = $this->getManager()->entityFromArray($entity_input, 'drupal.entity', 'node');
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $entity);
        $this->assertEquals('drupal.entity', $entity->getName());

        $this->assertEquals($entity_input['user[default]/name'], $entity['@references']['user']['default']['name']);
        $this->assertEquals($entity_input['user[default]/mail'], $entity['@references']['user']['default']['mail']);
    }

    /**
     * @expectedException RuntimeException
     * @dataProvider sourceSetReferencedEntityWrongly
     */
    public function testSetReferencedEntityWrongly($wrong_path)
    {
        $entity_input = [$wrong_path => 'MrTesting'];

        try {
            $entity = $this->getManager()->entityFromArray($entity_input, 'drupal.entity', 'node');
        }
        catch (RuntimeException $e) {
            $this->assertStringMatchesFormat("Can not set referenced value: {$wrong_path}", $e->getMessage());
            throw $e;
        }
    }

    public function sourceSetReferencedEntityWrongly()
    {
        return [
            ['user[default]//name'],
            ['user[default]/name/'],
            ['user[default]/[name]'],
            ['user/[default]/[name]'],
            ['user/default/[name]'],
        ];
    }

}
