<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Drupal;

use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntityQuery;
use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;

/**
 * @group DrupalClient
 */
class DrupalClientTest extends DrupalTestCase
{

    public function getClient()
    {
        return new DrupalClient();
    }

    public function testInit()
    {
        $client = $this->getClient();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\ClientInterface', $client);
    }

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Can not login to remote Drupal instance. Look like service.module is not configured correctly on remote Drupal.
     */
    public function testGetRemoteTokenFailed()
    {
        $client = $this->getClient();
        $mock_subscriber = new MockPlugin();
        $mock_subscriber->addResponse(new Response(200, [], '<html><title></title><body>Hello HTML!</body></html>'));
        $client->addSubscriber($mock_subscriber);

        $client->doLoad(111, 'node');
    }

    public function testLoad()
    {
        $client = $this->getClient();

        $mock_subscriber = new MockPlugin();
        $mock_subscriber->addResponse($res1 = new Response(200, [], '{ "token": "SAMPLE TOKEN" }'));
        $mock_subscriber->addResponse($res2 = new Response(200, [], '{  }'));
        $client->addSubscriber($mock_subscriber);

        $this->assertFalse($client->doLoad(999, 'node', []));
    }

    public function testDoCreate()
    {
        $client = $this->getClient();

        // Mock & inject responses
        $mock_subscriber = new MockPlugin();
        # $mock_subscriber->addResponse($res1 = new Response(200, [], '{ "token": "SAMPLE TOKEN" }'));
        $mock_subscriber->addResponse($res2 = new Response(200, [], '{ "nid": 123, "title": "Node title", "body": { "und": [ { "value": "Node body" } ] } }'));
        $client->addSubscriber($mock_subscriber);

        // Create dummy entity
        $input = ['title' => 'Node title', 'body' => ['value' => 'Node body']];
        $entity = $this->getManager()->entityFromArray($input, 'drupal.entity', 'node');

        // Action
        $saved_entity = $client->doCreate($entity, 'node');
        $this->assertEquals($res2->json(), $saved_entity);
    }

    public function testDoUpdate()
    {
        $client = $this->getClient();
        $mock_subscriber = new MockPlugin();
        $mock_subscriber->addResponse($res2 = new Response(200, [], '{ "nid": 123, "rid": 124, "title": "Node is updated" }'));
        $client->addSubscriber($mock_subscriber);

        // Dummy entity
        $input = ['nid' => 123, 'rid' => 123, 'title' => 'Original title'];
        $entity = $this->getManager()->entityFromArray($input, 'drupal.entity', 'node');

        // Action
        $updated_entity = $client->doUpdate($entity, 123, 'node');
        $this->assertEquals(124, $updated_entity['rid']);
    }

    public function testDoUpdateWithReferencedEntity()
    {
        // Mock DrupalService
        $service = $this->getMock('GoCatalyze\SyncCenter\Extensions\Drupal\DrupalService');
        $service->expects($this->once())
            ->method('getRemoteEntityId')
            ->with($this->anything(), $this->equalTo('user'))
            ->willReturn(222)
        ;

        $client = $this->getClient();
        $mock_subscriber = new MockPlugin();

        // #1 query to find ref-entity action
        $mock_subscriber->addResponse($res1 = new Response(200, [], '{ "uid": 222, "name": "Mr Testing", "updated": 234 }'));
        // #2 update user entity
        $mock_subscriber->addResponse($res2 = new Response(200, [], '{ "name": "Mr Testing", "updated": 234 }'));
        // #3 update node
        $mock_subscriber->addResponse($res3 = new Response(200, [], '{ "nid": 123, "rid": 124, "title": "Node is updated" }'));
        $client->addSubscriber($mock_subscriber);
        $client->setService($service);

        $input = ['nid' => 123, 'rid' => 123, 'title' => 'Original title'];
        $input += ["user[default]/name" => 'Mr Testing'];
        $entity = $this->getManager()->entityFromArray($input, 'drupal.entity', 'node');

        // Action
        $updated_entity = $client->doUpdate($entity, 123, 'node');
        $this->assertEquals(124, $updated_entity['rid']);
        $this->assertEquals(234, $updated_entity['@references']['user']['default']['updated']);
    }

    public function testDoUpdateWithReferenceEntityCaseCreate()
    {
        $client = $this->getClient();
        $mock_subscriber = new MockPlugin();

        // #1 query to find ref-entity action, return empty result, client
        // should create create new ref-entity
        $mock_subscriber->addResponse($res1 = new Response(200, [], '{}'));
        // #2 create user entity
        $mock_subscriber->addResponse($res2 = new Response(200, [], '{ "uid": 222, "name": "Mr Testing", "updated": 234 }'));
        // #3 update node
        $mock_subscriber->addResponse($res3 = new Response(200, [], '{ "nid": 123, "rid": 124, "title": "Node is updated" }'));
        $client->addSubscriber($mock_subscriber);

        $input = ['nid' => 123, 'rid' => 123, 'title' => 'Original title'];
        $input += ["user[default]/name" => 'Mr Testing'];
        $entity = $this->getManager()->entityFromArray($input, 'drupal.entity', 'node');

        // Action
        $updated_entity = $client->doUpdate($entity, 123, 'node');
        $this->assertEquals(124, $updated_entity['rid']);
        $this->assertEquals(234, $updated_entity['@references']['user']['default']['updated']);
    }

    public function testDoQuery()
    {
        $client = $this->getClient();
        $mock_subscriber = new MockPlugin();
        $mock_subscriber->addResponse(new Response(404, [], 'No entity found.'));
        $client->addSubscriber($mock_subscriber);

        // Action: Expected error
        $this->assertEquals([], $client->doQuery(new DrupalEntityQuery(), 'node'));

        // Action: Unexpected error
        $mock_subscriber->addResponse(new Response(405));
        $this->setExpectedException('Guzzle\Http\Exception\ClientErrorResponseException');
        $client->doQuery(new DrupalEntityQuery(), 'node');
    }

}
