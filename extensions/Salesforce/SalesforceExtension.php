<?php

namespace GoCatalyze\SyncCenter\Extensions\Salesforce;

use GoCatalyze\SyncCenter\BaseExtension;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntity;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntityQuery;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceService;

class SalesforceExtension extends BaseExtension
{

    protected $extension_name = 'salesforce';

    public function getServices()
    {
        return [
            new SalesforceService($this->manager)
        ];
    }

    public function getEntityTypes()
    {
        return [new SalesforceEntity()];
    }

    public function getEntityQueries()
    {
        return [new SalesforceEntityQuery()];
    }

}
