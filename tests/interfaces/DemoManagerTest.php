<?php

namespace GoCatalyze\SyncCenter\Testing;

use GoCatalyze\SyncCenter\Testing\Fixtures\DemoExtension;
use GoCatalyze\SyncCenter\Testing\Fixtures\DemoManager;
use GoCatalyze\SyncCenter\Testing\Fixtures\Entity\SourceEntity;
use GoCatalyze\SyncCenter\Testing\Fixtures\EntityQuery\SourceEntityQuery;
use GoCatalyze\SyncCenter\Testing\Fixtures\Service\DemoService;
use RuntimeException;
use Symfony\Component\Validator\Exception\OutOfBoundsException;

/**
 * @group demoManager
 */
class DemoManagerTest extends InterfacesTestCase
{

    private function getDemoManager()
    {
        return new DemoManager();
    }

    public function testManager()
    {
        $manager = $this->getDemoManager();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\ManagerInterface', $manager);
        $this->assertInstanceOf('GoCatalyze\SyncCenter\ExtensionInterface', $manager->getExtension('demo'));
        $this->assertInstanceOf('GoCatalyze\SyncCenter\ServiceInterface', $manager->getService('demo.service'));
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $manager->getEntityType('demo.source'));
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $manager->getEntityType('demo.destination'));
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface', $manager->getEntityQuery('demo.source'));
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface', $manager->getEntityQuery('demo.dest'));
        $this->assertCount(1, $manager->getExtensions());
        $this->assertCount(2, $manager->getEntityTypeNames());
        $this->assertCount(2, $manager->getEntityQueries());
    }

    public function testGetService()
    {
        $manager = $this->getDemoManager();
        $this->assertEquals(['demo.service'], $manager->getServiceNames(), 'Get name of registered services');
    }

    /**
     * @expectedException OutOfBoundsException
     * @expectedExceptionMessage Service is not registered: demo.invalid_service_name
     */
    public function testGetInvalidService()
    {
        $manager = $this->getDemoManager();
        $this->assertNull($manager->getService('demo.invalid_service_name'), 'Get invalid service');
    }

    public function testGetServiceWithOptions()
    {
        $manager = $this->getDemoManager();
        $service = $manager->getService('demo.service', $options = ['foo' => 'bar']);
        $this->assertEquals($options, $service->getConfiguration());
    }

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Can not override already registered service: demo.service
     */
    public function testOverrideService()
    {
        $manager = $this->getDemoManager();
        $manager->registerService(new DemoService());
    }

    public function testExtension()
    {
        $manager = $this->getDemoManager();
        $this->assertEquals(['demo'], $manager->getExtensionNames(), 'Get name of registered services.');
    }

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Can not override already registered extension: demo
     */
    public function testOverrideExtension()
    {
        $manager = $this->getDemoManager();
        $manager->registerExtension(new DemoExtension());
    }

    public function testGetEntityType()
    {
        $manager = $this->getDemoManager();
        $this->assertEquals(['demo.source', 'demo.destination'], $manager->getEntityTypeNames(), 'Get name of registered entity type.');
    }

    /**
     * @expectedException OutOfBoundsException
     * @expectedExceptionMessage Content type is not registered: demo.invalid_entity_type
     */
    public function testGetInvalidEntityType()
    {
        $manager = $this->getDemoManager();
        $manager->getEntityType('demo.invalid_entity_type');
    }

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Can not override already registered entity type: demo.source
     */
    public function testOverrideEntityType()
    {
        $manager = $this->getDemoManager();
        $manager->registerEntityType(new SourceEntity());
    }

    /**
     * @expectedException OutOfBoundsException
     * @expectedExceptionMessage Entity type is not registered: demo.invalid_entity_query
     */
    public function testGetInvalidEntityQuery()
    {
        $manager = $this->getDemoManager();
        $this->assertNull($manager->getEntityQuery('demo.invalid_entity_query'));
    }

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Can not override already registered entity query: demo.source
     */
    public function testOverrideEntityQuery()
    {
        $manager = $this->getDemoManager();
        $this->assertEquals(['demo.source', 'demo.dest'], $manager->getEntityQueryNames());
        $manager->registerEntityQuery(new SourceEntityQuery());
    }

}
