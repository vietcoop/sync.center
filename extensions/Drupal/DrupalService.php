<?php

namespace GoCatalyze\SyncCenter\Extensions\Drupal;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient;
use GoCatalyze\SyncCenter\Extensions\Drupal\Helper\DrupalBaseService;

/**
 * @todo Drupal endpoint /entity should be injectable.
 */
class DrupalService extends DrupalBaseService
{

    /** @var array */
    protected $config_schema = [
        'host'      => ['label' => 'Host name', 'type' => 'url', 'required' => true],
        'name'      => ['label' => 'Username', 'type' => 'string', 'required' => true],
        'pass'      => ['label' => 'Password', 'type' => 'string', 'required' => true],
        'endpoints' => [
            'label' => 'Endpoints',
            'type'  => 'array',
            'array' => [
                'key'   => ['label' => 'Remote entity', 'type' => 'string'],
                'value' => [
                    'label'    => 'Method Mapping',
                    'type'     => 'sequence',
                    'sequence' => [
                        'key'   => ['label' => 'HTTP Method', 'type' => 'choices', 'choices' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']],
                        'value' => ['label' => 'Endpoint', 'type' => 'string']
                    ]
                ]
            ]]
    ];

    /**
     * {@inheritdoc}
     * @return DrupalClient
     */
    protected function getDefaultClient()
    {
        $client = new DrupalClient();
        $client->setBaseUrl($this->config['host']);
        $client->setConfig($this->config);
        $client->setService($this);
        $this->setClient($client);

        // Inject entity/field info to client
        // @codeCoverageIgnoreStart
        if (!isset($this->config['entity_info'])) {
            $this->config['entity_info'] = $this->getRemoteEntityInfo();
            $this->client->setConfig($this->config);
        }

        return $this->client;
        // @codeCoverageIgnoreEnd
    }

    /**
     * {@inheritdoc}
     * @param string $type
     */
    public function getRemoteEntityIdKey($type)
    {
        return $this->getRemoteEntityInfo($type)['entity keys']['id'];
    }

    /**
     * {@inheritdoc}
     * @param DrupalEntityQuery $query
     * @param string $type
     * @return array
     */
    public function query(EntityQueryBuilderInterface $query, $type)
    {
        if (isset($this->config['is_views_resource'])) {
            $query->setIsViewsResource($this->config['is_views_resource']);
        }
        return parent::query($query, $type);
    }

    /**
     * {@inheritdoc}
     * @param EntityInterface $entity The entity object to be pushed to remote service.
     * @param string $type
     */
    public function loadFull(EntityInterface $entity, $type, EntityMappingInterface $mapping, array $uniques = [])
    {
        return $this->getEntityFullLoader()->load($entity, $type, $mapping, $uniques);
    }

    /**
     * Get remote entity info.
     *
     * @param string $type
     * @return array
     */
    public function getRemoteEntityInfo($type = null, $refresh = false)
    {
        $entity_info = $this->getAllRemoteEntityInfo($refresh);
        if (null !== $type) {
            return isset($entity_info[$type]) ? $entity_info[$type] : null;
        }
        return $entity_info;
    }

    /**
     * {@inheritdoc}
     * @param EntityInterface $entity
     * @param string $type
     * @param EntityMappingInterface $mapping
     * @param string[] $uniques
     */
    public function merge(EntityInterface $entity, $type, EntityMappingInterface $mapping, array $uniques)
    {
        if ($full_entity = $this->loadFull($entity, $type, $mapping, $uniques)) {
            /* @var $full_entity DrupalEntity */
            $id_key = $this->getRemoteEntityIdKey($type);
            if ($id_val = $full_entity->getAttributeValue($id_key)) {
                $entity[$id_key] = $id_val;
                if (!$return = $this->update($entity, $id_val, $type)) {
                    return $entity->getAttributes();
                }
                return $return;
            }
        }
        return $this->create($entity, $type);
    }

}
