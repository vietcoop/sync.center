<?php

namespace GoCatalyze\SyncCenter\Extensions\Drupal;

use GoCatalyze\SyncCenter\BaseClient;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use Guzzle\Common\Exception\RuntimeException as GuzzleRuntimeException;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Guzzle\Plugin\Cookie\CookieJar\ArrayCookieJar;
use Guzzle\Plugin\Cookie\CookiePlugin;
use RuntimeException;

class DrupalClient extends BaseClient
{

    /**
     * @var DrupalService
     */
    private $service;

    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * Get endpoint for an entity/method.
     *
     * @param string $type
     * @param string $method
     * @return string
     */
    private function getEndpoint($type, $method)
    {
        $endpoints = $this->getConfig('endpoints');
        $suffix = '';

        if (isset($endpoints[$type][$method])) {
            return $endpoints[$type][$method];
        }

        if (preg_match('/\/\d+$/', $type)) {
            $chunk = explode('/', $type);
            $type = $chunk[0];
            $suffix = "/" . end($chunk);
        }

        if (in_array($type, ['node', 'comment', 'file', 'user', 'taxonomy_term'])) {
            return 'entity_' . $type . $suffix;
        }

        $return = FALSE === strpos($type, '/') ? "entity_{$type}" : $type;
        return $return . $suffix;
    }

    private function getRemoteToken()
    {
        static $token = null;

        if (null === $token) {
            $this->addSubscriber(new CookiePlugin(new ArrayCookieJar()));

            try {
                $body = ['username' => $this->getConfig('name'), 'password' => $this->getConfig('pass')];
                $response = $this->send($this->post('user/login', [], $body))->json();
                $token = trim($response['token']);
            }
            catch (GuzzleRuntimeException $e) {
                if (false !== \strpos($e->getMessage(), 'Unable to parse response body into JSON')) {
                    $msg = 'Can not login to remote Drupal instance. Look like service.module is not configured correctly on remote Drupal.';
                    throw new RuntimeException($msg);
                }
            }
        }

        return $token;
    }

    /**
     * {@inheritdoc}
     *
     * On making request, make sure
     *  - all have have valid token in header.
     *  - configured endpoint is setup correctly.
     *
     */
    public function createRequest($method = 'GET', $uri = null, $headers = null, $body = null, array $options = [])
    {
        $headers = is_array($headers) ? $headers : [];
        $headers += [
            'Accept'       => 'application/json',
            'Content-Type' => 'application/json'
        ];

        // case of auth
        if (empty($headers['X-CSRF-Token']) && ($uri !== 'user/login') && ($uri !== 'user/register')) {
            $headers['X-CSRF-Token'] = $this->getRemoteToken();
        }

        $entity_type = $uri;
        if (isset($options['entity_type'])) {
            $entity_type = $options['entity_type'];
        }

        return parent::createRequest($method, $this->getEndpoint($entity_type, $method), $headers, $body, $options);
    }

    /**
     * {@inheritdoc}
     * @TODO Aware $extra_fields
     * @param int $id
     * @param string $type
     * @param string[] $extra_fields
     */
    public function doLoad($id, $type, array $extra_fields = [])
    {
        $request = $this->get("{$type}/{$id}");
        if ($return = $this->send($request)->json()) {
            $entity = new DrupalEntity();
            $entity->setAttributeValues($return);
            return $entity;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     * @param EntityInterface $entity
     * @param string $type
     * @return array
     */
    public function doCreate(EntityInterface $entity, $type = '')
    {
        $entity_type = !empty($type) ? $type : $entity->getAttributeValue('entity_type');
        $request = $this->post($entity_type, [], $entity->getAttributes());
        return $this->send($request)->json();
    }

    private function getRemoteEntityIdKey($remote_entity_type = '')
    {
        $keys = ['node' => 'nid', 'user' => 'uid', 'file' => 'fid', 'taxonomy_term' => 'tid', 'profile2' => 'pid'];
        return isset($keys[$remote_entity_type]) ? $keys[$remote_entity_type] : 'id';
    }

    public function getRemoteEntityId(EntityInterface $entity, $remote_entity_type = '')
    {
        $id_key = $this->getRemoteEntityIdKey($remote_entity_type);
        if ($id = $entity->getAttributeValue($id_key)) {
            return $id;
        }
        throw new RuntimeException('Require ID value to update remote entity.');
    }

    /**
     * {@inheritdoc}
     * @param DrupalEntity $entity
     * @param string $type
     * @return array
     */
    public function doUpdate(EntityInterface $entity, $id, $type)
    {
        /* @var $entity DrupalEntity */
        $body = $entity->getAttributes();
        $ref_response = [];

        if (isset($body['@references'])) {
            $source_entity_id_key = $this->getRemoteEntityIdKey($type);
            $source_entity_id = $this->getRemoteEntityId($entity, $type);
            foreach ($body['@references'] as $_remote_entity_type => $_references) {
                foreach ($_references as $_bundle => $_values) {
                    $ref_response[$_remote_entity_type][$_bundle] = $this->mergeReferenceEntity($source_entity_id_key, $source_entity_id, $_remote_entity_type, $_bundle, $_values);
                }
            }
            unset($body['@references']);
        }

        $request = $this->put("{$type}/{$id}", [], $body);

        $response = $this->send($request)->json();

        if (!empty($ref_response)) {
            $response['@references'] = $ref_response;
        }

        return $response;
    }

    /**
     * Perform sub request to update parent entity's references.
     *
     * @param string $parent_entity_id_key
     * @param int $parent_entity_id
     * @param string $remote_entity_type
     * @param string $remote_bundle
     * @param array $input
     */
    private function mergeReferenceEntity($parent_entity_id_key, $parent_entity_id, $remote_entity_type, $remote_bundle, $input)
    {
        $remote_entity_info = $this->getConfig('entity_info')[$remote_entity_type];
        $f_id = $remote_entity_info['entity keys']['id'];
        $f_bundle = $remote_entity_info['entity keys']['bundle'];

        // query Drupal for entity
        $query = new DrupalEntityQuery();
        $query->setRemoteEntityType($remote_entity_type);
        $query->condition($parent_entity_id_key, $parent_entity_id);

        $find_entity = $this->doQuery($query, $remote_entity_type);
        $method = $find_entity ? 'update' : 'create';

        $input += [$parent_entity_id_key => $parent_entity_id, $f_bundle => $remote_bundle];
        if ($method === 'update') {
            $find_entity = reset($find_entity);
            $input[$f_id] = $find_entity[$f_id];
        }

        $entity = (new DrupalEntity())->setAttributeValues($input);

        if ($method === 'update') {
            $id = $this->service->getRemoteEntityId($entity, $remote_entity_type);
            return $this->doUpdate($entity, $id, $remote_entity_type);
        }

        return $this->doCreate($entity, $remote_entity_type);
    }

    /**
     * {@inheritdoc}
     * @param DrupalEntityQuery $query
     * @param string $type
     * @return array
     */
    public function doQuery(EntityQueryBuilderInterface $query, $type)
    {
        /* @var $query DrupalEntityQuery */
        $query->setRemoteEntityType($type);

        try {
            // entity/node.json?fields=nid,uid,title,changed&parameters[status]=1
            $request = $this->get((string) $query, [], ['entity_type' => $type]);

            // some Drupal module return error message in body
            $entities = $this->send($request)->json();
        }
        catch (ClientErrorResponseException $e) {
            /* @var $e ClientErrorResponseException */
            if (404 == $e->getResponse()->getStatusCode()) {
                return [];
            }

            throw $e;
        }

        return !empty($entities) ? $entities : [];
    }

}
