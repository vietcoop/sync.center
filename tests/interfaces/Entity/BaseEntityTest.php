<?php

namespace GoCatalyze\SyncCenter\Testing\Entity;

use DateTime;
use GoCatalyze\SyncCenter\Testing\Fixtures\Entity\SourceEntity;
use GoCatalyze\SyncCenter\Testing\InterfacesTestCase;
use Symfony\Component\Validator\Exception\RuntimeException;
use UnexpectedValueException;

/**
 * @group baseEntity
 */
class BaseEntityTest extends InterfacesTestCase
{

    private function getEntity()
    {
        return new SourceEntity();
    }

    public function testGetName()
    {
        $this->assertEquals('demo.source', $this->getEntity()->getName());
    }

    public function testGetRules()
    {
        $expected = ['foo', 'source_boolean', 'source_string', 'source_timestamp'];
        $actual = array_keys($this->getEntity()->getRules());
        $this->assertEquals($expected, $actual);
    }

    public function testInjectRules()
    {
        $entity = $this->getEntity();

        $rules[] = ['type' => 'bool', 'required' => true];
        $entity->setRules($rules);
        $this->assertEquals($rules, $entity->getRules());
    }

    public function testInjectUniqueFields()
    {
        $entity = $this->getEntity();
        $entity->setUniqueFields(['name']);
        $this->assertEquals(['name'], $entity->getUniqueFields());
    }

    public function testValidateAttributeWithoutRule()
    {
        $entity = $this->getEntity();
        $entity->setRules([]); // clear all rules
        $entity->setAttributeValue('no_rule_for_this_attribute', 'No problem (:');
    }

    public function testValidateAttributeRequireOptionTrue()
    {
        $entity = $this->getEntity();
        $rules['title'] = ['required' => true];
        $entity->setRules($rules);
        $entity->setAttributeValue('title', 'Simple title');
    }

    /**
     * @expectedException UnexpectedValueException
     * @expectedExceptionMessage Expecting a valid value give for title attribute, but null given.
     */
    public function testValidateAttributeRequireOptionFalse()
    {
        $entity = $this->getEntity();
        $rules['title'] = ['required' => true];
        $entity->setRules($rules);
        $entity->setAttributeValue('title', null);
    }

    public function testValidateAttributeTypeCustomClassName()
    {
        $entity = $this->getEntity();
        $entity->setRules(['created_at' => ['type' => 'DateTime']]);
        $entity->setAttributeValue('created_at', new DateTime());
    }

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Unsupported attribute type: very_strange_type
     */
    public function testValidateAttributeTypeInvalidType()
    {
        $entity = $this->getEntity();
        $entity->setRules(['created_at' => ['type' => 'very_strange_type']]);
        $entity->setAttributeValue('created_at', new DateTime());
    }

    /**
     * @group andydebug
     */
    public function testArrayAccess()
    {
        $entity = $this->getManager()->entityFromArray([
            'nid' => 1,
            'title' => 'Node One',
            'body' => ['und' => [0 => ['value' => 'Node body']]]
        ], 'demo.source', 'node');

        $entity['status'] = 1;
        $entity['promoted'] = 1;
        unset($entity['promoted']);

        $this->assertArrayHasKey('nid', $entity);
        $this->assertEquals(1, $entity['nid']);
        $this->assertEquals(1, $entity['status']);
        $this->assertArrayNotHasKey('promoted', $entity);
    }

}
