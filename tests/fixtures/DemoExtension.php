<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures;

use GoCatalyze\SyncCenter\BaseExtension;
use GoCatalyze\SyncCenter\Testing\Fixtures\Entity\DestEntity;
use GoCatalyze\SyncCenter\Testing\Fixtures\Entity\SourceEntity;
use GoCatalyze\SyncCenter\Testing\Fixtures\Service\DemoService;

class DemoExtension extends BaseExtension
{
    protected $extension_name = 'demo';

    public function getEntityTypes()
    {
        return [
            new SourceEntity(),
            new DestEntity()
        ];
    }

    public function getServices()
    {
        return [
            new DemoService()
        ];
    }

    public function getEntityQueries()
    {
        return [
            new EntityQuery\SourceEntityQuery(),
            new EntityQuery\DestEntityQuery()
        ];
    }

}
