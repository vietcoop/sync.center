<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Salesforce\Helper;

use GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\ReferenceFieldConvertor\ContactReferenceConvertor;
use GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil;
use GoCatalyze\SyncCenter\Extensions\Testing\Salesforce\SalesforceTestCase;
use Guzzle\Http\Message\Request;
use Guzzle\Http\Message\Response;

/**
 * @group ContactReferenceConvertor
 */
class ContactReferenceConvertorTest extends SalesforceTestCase
{

    public function getCovnertor()
    {
        $util = $this->getMock('GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil');
        $util->expects($this->any())->method('refreshToken');
        return new ContactReferenceConvertor($util);
    }

    public function testConvertHasResult()
    {
        $convertor = $this->getCovnertor();

        // Dummy
        $contact_name = 'John English';

        // Mocking
        $url = strtr('services/data/v%version/query?q=%sql', [
            '%version' => $convertor->getUtil()->getVersion(),
            '%sql'     => urlencode("SELECT Id FROM Contact WHERE Name='{$contact_name}'")
        ]);
        $headers = ['Content-Type' => 'application/json', 'Authorization' => 'Bearer '];
        $request = new Request('GET', $url, $headers);

        $find_client = $this->getMock('Guzzle\Http\Client');
        $find_client->expects($this->once())
            ->method('get')
            ->with($url, $headers)
            ->willReturn($request)
        ;
        $find_client->expects($this->once())
            ->method('send')
            ->with($request)
            ->willReturn(new Response(200, [], '{
                "totalSize": 1,
                "records": [ { "Id": 222 }, { "Id": 333 } ]
            }'))
        ;

        // Action: convert "John English" to ID number (222)
        $response = $convertor->convert($contact_name, $find_client);

        // Checking
        $this->assertEquals(222, $response);
    }

    public function testConvertHasNoResult()
    {
        // Dummy
        $convertor = $this->getCovnertor();
        $contact_name = 'John English';

        // Mocking
        $create_client = $this->getMock('Guzzle\Http\Client');
        $url = strtr('services/data/v%version/query?q=%sql', [
            '%version' => $convertor->getUtil()->getVersion(),
            '%sql'     => urlencode("SELECT ID FROM Contact WHERE name='{$contact_name}'")
        ]);
        $headers = ['Content-Type' => 'application/json', 'Authorization' => 'Bearer '];
        $request = new Request('GET', $url, $headers);
        $create_url = sprintf('services/data/v%s/sobjects/Contact/', $convertor->getUtil()->getVersion());
        $create_request = new Request('POST', $create_url, $headers);
        $create_client->expects($this->once())
            ->method('post')
            ->with($create_url, $headers)
            ->willReturn($create_request);
        $create_client->expects($this->atLeastOnce())
            ->method('send')
            ->with($create_request)
            ->willReturn(new Response(200, [], json_encode($this->dummyFullEntiytContact()->getAttributes())));

        // Action: convert "John English" to ID number (222)
        $response = $convertor->create($contact_name, $create_client);

        // Checking
        $this->assertEquals(112233, $response);
    }

}
