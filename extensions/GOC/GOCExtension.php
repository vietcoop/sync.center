<?php

namespace GoCatalyze\SyncCenter\Extensions\GOC;

use GoCatalyze\SyncCenter\BaseExtension;

class GOCExtension extends BaseExtension
{

    /** @var string */
    protected $extension_name = 'goc';

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @return EntityInterface[]
     */
    public function getEntityTypes()
    {
        return [new GOCEntity()];
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @return ServiceInterface[]
     */
    public function getServices()
    {
        return [new GOCService()];
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @return EntityQueryInterface[]
     */
    public function getEntityQueries()
    {
        return [new GOCEntityQuery()];
    }

}
