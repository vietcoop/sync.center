<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Drupal;

use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalExtension;
use PHPUnit_Framework_TestCase;

/**
 * @group DrupalExtension
 */
class DrupalExtensionTest extends PHPUnit_Framework_TestCase
{

    private function getExtension()
    {
        return new DrupalExtension();
    }

    public function testExtensionInitial()
    {
        $ext = $this->getExtension();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\ExtensionInterface', $ext);

        $this->assertInstanceOf('GoCatalyze\SyncCenter\ServiceInterface', $ext->getServices()[0]);
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $ext->getEntityTypes()[0]);
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface', $ext->getEntityQueries()[0]);
    }

}
