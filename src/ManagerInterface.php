<?php

namespace GoCatalyze\SyncCenter;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertor;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertorInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use GoCatalyze\SyncCenter\ServiceInterface;

interface ManagerInterface
{

    /**
     * Get all registered extensions.
     *
     * @return ExtensionInterface[]
     */
    public function getExtensions();

    /**
     * Get extension names.
     *
     * @return string[]
     */
    public function getExtensionNames();

    /**
     * Get extension by name.
     *
     * @param string $name
     * @return ExtensionInterface
     */
    public function getExtension($name);

    /**
     * Register an extension.
     *
     * @param ExtensionInterface $extension
     */
    public function registerExtension(ExtensionInterface $extension);

    /**
     * Register an entity type.
     *
     * @param EntityInterface $entity_type
     */
    public function registerEntityType(EntityInterface $entity_type);

    /**
     * Register a service.
     *
     * @param ServiceInterface $service
     */
    public function registerService(ServiceInterface $service);

    /**
     * Register an entity query.
     *
     * @param EntityQueryBuilderInterface $query
     */
    public function registerEntityQuery(EntityQueryBuilderInterface $query);

    /**
     * Get registered entity query interfaces.
     *
     * @return EntityQueryBuilderInterface[]
     */
    public function getEntityQueries();

    /**
     * Get name of registered entity queries
     *
     * @return string[]
     */
    public function getEntityQueryNames();

    /**
     * Get query for a entity type.
     *
     * @param string $entity_type
     * @return EntityQueryBuilderInterface
     */
    public function getEntityQuery($entity_type);

    /**
     * Get entity type object.
     *
     * @param string $name
     * @return EntityInterface
     */
    public function getEntityType($name);

    /**
     * Get service by name.
     *
     * @param string $name    Name of service
     * @param array $options  Options for service (auth, endpoints, …)
     * @return ServiceInterface
     */
    public function getService($name, array $options);

    /**
     * Get name for available services
     *
     * @return string[]
     */
    public function getServiceNames();

    /**
     * Set entity convertor
     */
    public function setEntityConvertor(EntityConvertorInterface $convertor);

    /**
     * Get entity convertor. This object will help user to cast an entity
     * from this type to an other one with a simple mapping. For example:
     *
     *  $manager  = new TestManager();
     *  $manager->registerExtension($this->getMock('GO1\GoSync\Drupal\Extension'));
     *  $manager->registerExtension($this->getMock('GO1\GoSync\Saleforce\Extension'));
     *
     * @return EntityConvertor
     */
    public function getEntityConvertor();

    /**
     * Get all (currently) supported entity type names.
     * We can add more extension to support more entity types.
     *
     * @return string[]
     */
    public function getEntityTypeNames();

    /**
     * Convert entity to array.
     *
     * @param EntityInterface $entity
     * @param string $remote_entity_type
     */
    public function entityToArray(EntityInterface $entity, $remote_entity_type);

    /**
     * Create new entity object from array.
     *
     * @param array $input                 Input array
     * @param string $entity_type          System entity type, e.g. `drupal.entity`, …
     * @param string $remote_entity_type   Remote entiyt type, e.g. `node`, `user`, …
     */
    public function entityFromArray(array $input, $entity_type, $remote_entity_type);
}
