<?php

namespace GoCatalyze\SyncCenter\Entity;

use ArrayAccess;

interface EntityInterface extends ArrayAccess
{

    /**
     * Get entity type name. Example: drupal.user
     *
     * @return string
     */
    public function getName();

    /**
     * Get all entity's attributes.
     *
     * @return array
     */
    public function getAttributes();

    /**
     * Setter for entity's attribute (property/field).
     *
     * @param string $attr_name
     * @param mixed $attr_value
     */
    public function setAttributeValue($attr_name, $attr_value);

    /**
     * Set multiple attributes at once.
     *
     * @param array $attributes
     * @return EntityInterface
     */
    public function setAttributeValues(array $attributes);

    /**
     * Getter for entity's attribute.
     *
     * @param mixed $attr_name
     */
    public function getAttributeValue($attr_name);

    /**
     * Validate input value for a specific attribute.
     *
     * @param string $key
     * @param mixed $value
     */
    public function validateAttribute($key, $value);

    /**
     * Get rules.
     *
     * @return array keyed array
     *  $attribute_id => array(
     *      'required'     => boolean,
     *      'type'         => string,
     *  )
     *
     * $attribute_id -> type can be
     *   - mixed
     *   - bool
     *   - int
     *   - float
     *   - string
     *   - or any class implements \GoCatalyze\SyncCenter\EntityAttributeValidatorInterface
     */
    public function getRules();

    /**
     * Set rules.
     */
    public function setRules(array $rules);

    /**
     * Validate entity object.
     *
     * @param array $input
     * @param array $error
     */
    public function validate(array $input, array &$error);
}
