<?php

namespace GoCatalyze\SyncCenter\Extensions\Drupal\Helper;

use GoCatalyze\SyncCenter\BaseService;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity;
use GoCatalyze\SyncCenter\Extensions\Drupal\Helper\EntityFullLoader;

class DrupalBaseService extends BaseService
{

    /** @var string Name of service */
    protected $service_name = 'drupal';

    /** @var DrupalClient */
    protected $client = null;

    /** @var EntityFullLoader */
    protected $remote_entity_loader;

    public function setConfiguration(array $config)
    {
        if (isset($config['entity_info'])) {
            $this->remote_entity_info = $config['entity_info'];
            unset($config['entity_info']);
        }

        $this->config = $config;
    }

    /**
     * Get Remote entity finder.
     *
     * @return EntityFullLoader
     */
    public function getEntityFullLoader()
    {
        if (null === $this->remote_entity_loader) {
            $this->setEntityFullLoader(new EntityFullLoader($this));
        }
        return $this->remote_entity_loader;
    }

    /**
     * Set remote entity finder.
     *
     * @param EntityFullLoader $remote_entity_finder
     */
    public function setEntityFullLoader($remote_entity_finder)
    {
        $this->remote_entity_loader = $remote_entity_finder;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getAllRemoteEntityInfo($refresh = false)
    {
        if (!empty($this->remote_entity_info) && !empty($this->config['entity_info'])) {
            $this->remote_entity_info = $this->config['entity_info'];
            unset($this->config['entity_info']);
        }

        if ($refresh || empty($this->remote_entity_info)) {
            // Get basic entity info
            $einfo = $this->getClient()->createRequest("POST", "system/entity_info")->send()->json();
            foreach ($this->getAllRemoteFieldInfoInstances() as $type => $field_instances_info) {
                $einfo[$type]["entity_type"] = $type;
                foreach ($field_instances_info as $bundle => $fii) {
                    foreach ($fii as $fname => $finfo) {
                        $einfo[$type]['fields'][$fname] = $finfo;
                    }
                }
            }
            $this->remote_entity_info = $einfo;
        }

        return $this->remote_entity_info;
    }

    /**
     * Get field info instances.
     *
     * @return array
     */
    protected function getAllRemoteFieldInfoInstances()
    {
        return $this->getClient()
                ->createRequest('POST', 'system/field_info_instances')
                ->send()
                ->json();
    }

    /**
     * Check is an attr_name a Drupal field.
     *
     * @param string $attr_name
     * @param string $type
     * @return boolean
     */
    protected function isDrupalFieldName($attr_name, $type)
    {
        if (!$e_info = $this->getRemoteEntityInfo($type)) {
            return false;
        }

        return isset($e_info['fields'][$attr_name]);
    }

    /**
     * We have to Drupalize array structure before pushing it to Drupal instance.
     *
     * @param DrupalEntity $entity
     * @param string $remote_entity_type
     * @return array
     */
    public function drupalize(DrupalEntity $entity, $remote_entity_type)
    {
        $array = [];
        foreach ($entity->getAttributes() as $attr_name => $attr_value) {
            if ($this->isDrupalFieldName($attr_name, $remote_entity_type)) {
                $array[$attr_name] = ['und' => [0 => $attr_value]];
            }
            else {
                $array[$attr_name] = $attr_value;
            }
        }
        return $array;
    }

}
