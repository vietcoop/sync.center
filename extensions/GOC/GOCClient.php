<?php

namespace GoCatalyze\SyncCenter\Extensions\GOC;

use GoCatalyze\SyncCenter\BaseClient;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use Guzzle\Plugin\Cookie\CookieJar\ArrayCookieJar;
use Guzzle\Plugin\Cookie\CookiePlugin;

class GOCClient extends BaseClient
{

    public function getRemoteToken()
    {
        static $token = null;

        if (null === $token) {
            $this->addSubscriber(new CookiePlugin(new ArrayCookieJar()));

            $credentials = json_encode([
                'instance' => $this->getConfig('instance'),
                'username' => $this->getConfig('name'),
                'password' => $this->getConfig('pass')
            ]);

            $token = $this
                    ->createRequest('POST', 'account/login.json', [], $credentials)
                    ->send()
                    ->json()['api_token'];
        }

        return $token;
    }

    public function createRequest($method = 'GET', $uri = null, $headers = null, $body = null, array $options = array())
    {
        $headers = is_array($headers) ? $headers : [];
        $headers += [
            'Accept'       => 'application/json',
            'Content-Type' => 'application/json'
        ];

        $request = parent::createRequest($method, $uri, $headers, $body, $options);

        // case of auth
        if (empty($headers['X-CSRF-Token']) && ($uri !== 'account/login.json') && ($uri !== 'account/register.json')) {
            $request->getQuery()->set('api_key', $this->getRemoteToken());
        }

        return $request;
    }

    public function doCreate(EntityInterface $entity, $type)
    {
        list($entity_type, $bundle) = explode('.', $type);
        $path = "/entity/{$bundle}/{$entity_type}";

        // Drupal does not accept true/false for checkbox, convert to string "0"/"1"
        $body = [];
        foreach ($entity->getAttributes() as $k => $v) {
            if (is_bool($v)) {
                $v = $v ? "1" : "0";
            }
            $body[$k] = $v;
        }

        return $this
                ->createRequest('POST', $path, [], json_encode($body))
                ->send()
                ->json();
    }

    public function doLoad($id, $type, array $extra_fields = [])
    {
        list($entity_type, $bundle) = explode('.', $type);
        $path = "/entity/{$bundle}/{$entity_type}/{$id}.json";

        return $this->createRequest('GET', $path)
                ->send()
                ->json();
    }

    public function doDelete($id)
    {
        list($entity_type, $bundle) = explode('.', $type);
        $path = "/entity/{$bundle}/{$entity_type}/{$id}.json";

        return $this->createRequest('DELETE', $path)
                ->send()
                ->json();
    }

    public function doUpdate(EntityInterface $entity, $id, $type)
    {
        list($entity_type, $bundle) = explode('.', $type);
        $path = "/entity/{$bundle}/{$entity_type}/{$id}.json";

        return $this
                ->createRequest('PUT', $path, [], json_encode($entity->getAttributes()))
                ->send()
                ->json();
    }

    public function doQuery(EntityQueryBuilderInterface $query, $type)
    {
        list($entity_type, $bundle) = explode('.', $type);
        $path = "/entity/{$bundle}/{$entity_type}.json?" . (string) $query;

        return $this
                ->createRequest('GET', $path)
                ->send()
                ->json();
    }

}
