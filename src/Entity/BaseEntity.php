<?php

namespace GoCatalyze\SyncCenter\Entity;

use RuntimeException;
use UnexpectedValueException;

abstract class BaseEntity implements EntityInterface
{

    /** @var string */
    protected $entity_name;

    /** @var array Attributes */
    protected $attributes;

    /** @var string Remote entity type */
    protected $remote_entity_type;

    /** @var array */
    protected $remote_field_info;

    /**
     * @see EntityInterface::getRules()
     * @var array Attributes validating rules
     */
    protected $rules = [];

    /**
     * Unique fields. When sync between systems, we do not know what really need
     * to do, update or insert, then we have to do an extra step to get 'action':
     * Query remote system if entity existence.
     *
     * @var string[]
     */
    protected $unique_fields = [];

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName()
    {
        return $this->entity_name;
    }

    /**
     * {@inheritdoc}
     * @var array
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * {@inheritdoc}
     * @param array $rules
     * @return array
     */
    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getAttributes()
    {
        $array = [];

        // Do not return values directly here, allow final class to override
        // value, like Drupal, if get 'body', should convert to [und => [0 => [value => …]]]
        foreach (array_keys($this->attributes) as $attr_name) {
            $attr_value = $this->getAttributeValue($attr_name);
            if (null !== $attr_value) {
                $array[$attr_name] = $attr_value;
            }
        }

        return $array;
    }

    /**
     * {@inheritdoc}
     * @param array $attributes
     * @return EntityInterface
     */
    public function setAttributeValues(array $attributes)
    {
        foreach ($attributes as $attr_name => $attr_value) {
            $this->setAttributeValue($attr_name, $attr_value);
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string $attr_name
     * @return mixed|null
     */
    public function getAttributeValue($attr_name)
    {
        return isset($this->attributes[$attr_name]) ? $this->attributes[$attr_name] : null;
    }

    /**
     * {@inheritdoc}
     * @param string $attr_name
     * @param mixed $attr_value
     * @throws \UnexpectedValueException
     */
    public function setAttributeValue($attr_name, $attr_value)
    {
        if (!$this->validateAttribute($attr_name, $attr_value)) {
            throw new UnexpectedValueException(sprintf('Unexpected value for entity attribute: %s', $attr_name));
        }

        $this->attributes[$attr_name] = $attr_value;
    }

    /**
     * Get unique-fields.
     *
     * @return string[]
     */
    public function getUniqueFields()
    {
        return $this->unique_fields;
    }

    /**
     * Set unique-fields.
     *
     * @param string[] $unique_fields
     */
    public function setUniqueFields($unique_fields)
    {
        $this->unique_fields = $unique_fields;
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @param array $input
     * @param array $error
     * @return boolean
     */
    public function validate(array $input, array &$error)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     * @param string $attr_name
     * @param mixed $input
     * @return boolean
     */
    public function validateAttribute($attr_name, $input = null)
    {
        if (!isset($this->rules[$attr_name])) {
            return true;
        }

        $rule = $this->rules[$attr_name];

        // Validate 'required' option
        if (!empty($rule['required']) && is_null($input)) {
            $msg = sprintf('Expecting a valid value give for %s attribute, but null given.', $attr_name);
            throw new UnexpectedValueException($msg);
        }

        // Validate 'type' option
        if (!empty($rule['type'])) {
            return $this->validateAttributeType($input, $rule['type']);
        }

        return true;
    }

    protected function validateAttributeType($input, $expecting_type)
    {
        if (in_array($expecting_type, ['bool', 'float', 'int', 'integer', 'number', 'null', 'string', 'object'])) {
            $fn = "is_{$expecting_type}";
            return $fn($input);
        }

        if ('mixed' === $expecting_type) {
            return true;
        }

        if (class_exists($expecting_type)) {
            if (in_array('GoCatalyze\SyncCenter\Entity\EntityAttributeValidatorInterface', class_implements($expecting_type))) {
                return (new $expecting_type)->validate($input);
            }
            else {
                return is_object($input) && ($input instanceof $expecting_type);
            }
        }

        throw new RuntimeException(sprintf('Unsupported attribute type: %s', $expecting_type));
    }

    /**
     * {@inheritdoc}
     * @param string $offset
     * @return mixed
     */
    public function offsetExists($offset)
    {
        return isset($this->attributes[$offset]);
    }

    /**
     * {@inheritdoc}
     * @param string $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->getAttributeValue($offset);
    }

    /**
     * {@inheritdoc}
     * @param string $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        $this->setAttributeValue($offset, $value);
    }

    /**
     * {@inheritdoc}
     * @param string $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset]);
    }

}
