<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures\EntityQuery;

use GoCatalyze\SyncCenter\Entity\Query\BaseEntityQuery;

class SourceEntityQuery extends BaseEntityQuery
{
    public function getEntityTypeName()
    {
        return 'demo.source';
    }

}
