<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Salesforce;

use DateTime;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceClient;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntityQuery;

/**
 * @group SalesforceClient
 */
class SalesforceClientTest extends SalesforceTestCase
{

    public function getClient()
    {
        return new SalesforceClient();
    }

    public function testInit()
    {
        $client = $this->getClient();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\ClientInterface', $client);
    }

    public function testDefaultRealClient()
    {
        $client = $this->getClient();
        $client->setConfig(['instance_url' => 'http://127.0.0.1']);
        $expected = 'GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil';
        $this->assertInstanceOf($expected, $client->getRealClient());
    }

    public function testDoQuery()
    {
        $client = $this->getClient();

        $query = new SalesforceEntityQuery();
        $query->queryLatestEntities(new DateTime('- 30 minutes'), 'Contact');
        $_query = clone $query;
        $_query->fields(['Id', 'Name', 'Mail']);

        // Mock and inject real client
        $handler = $this->getMock('GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil');
        $handler->expects($this->any())
            ->method('getFields')
            ->with('Contact')
            ->willReturn(['Id', 'Name', 'Mail'])
        ;

        $handler
            ->expects($this->once())
            ->method('doQuery')
            ->with("services/data/v{$handler->getVersion()}/query?q=" . (string) $_query, 'Contact')
            ->willReturn(['records' => [['Id' => 'fake'], ['Id' => 'fake']]])
        ;
        $client->setRealClient($handler);

        // Action
        $response = $client->doQuery($query, 'Contact');

        // Checking
        $this->assertEquals('fake', $response[0]['Id']);
    }

    public function testDoCreate()
    {
        $client = $this->getClient();

        // Dummy
        $entity = $this->dummyEntityContact();
        $full_entity = $this->dummyFullEntiytContact();

        // Mock and inject real client
        $handler = $this->getMock('GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil');
        $handler
            ->expects($this->once())
            ->method('doCreate')
            ->with('Contact', $entity->getAttributes())
            ->willReturn($full_entity->getAttributes())
        ;
        $handler->expects($this->once())
            ->method('getObjectTypeInfo')
            ->with('Contact')
            ->willReturn(['fields' => []])
        ;
        $client->setRealClient($handler);

        // Action
        $response = $client->doCreate($entity, 'Contact');

        // Checking
        $this->assertEquals(112233, $response['Id']);
    }

}
