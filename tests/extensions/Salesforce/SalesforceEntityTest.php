<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Salesforce;

use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntity;

/**
 * @group SalesforceEntity
 */
class SalesforceEntityTest extends SalesforceTestCase
{

    public function getEntity()
    {
        return new SalesforceEntity();
    }

    public function testInit() {
        $entity = $this->getEntity();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Entity\EntityInterface', $entity);
    }

}
