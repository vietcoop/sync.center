<?php

namespace GoCatalyze\SyncCenter\Extensions\GOC\Helper;

use GoCatalyze\SyncCenter\Extensions\Drupal\Helper\EntityFullLoader as BaseEntityFullLoader;

class EntityFullLoader extends BaseEntityFullLoader
{

    /** @var string */
    protected $entity_class = 'GoCatalyze\SyncCenter\Extensions\GOC\GOCEntity';

    /** @var string */
    protected $query_class = 'GoCatalyze\SyncCenter\Extensions\GOC\GOCEntityQuery';

}
