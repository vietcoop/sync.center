<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures\Service;

use GoCatalyze\SyncCenter\BaseClient;
use GoCatalyze\SyncCenter\BaseService;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingInterface;

class DemoService extends BaseService
{
    /* @var string service name */

    protected $service_name = 'demo.service';

    /* @var mixed[] Mocking values */
    protected $return = [];

    protected function getDefaultClient()
    {
        return new BaseClient();
    }

    public function loadFull(EntityInterface $entity, $type, EntityMappingInterface $mapping, array $uniques = [])
    {
        return $this->return[__FUNCTION__];
    }

    public function getRemoteEntityIdKey($remote_entity_type)
    {
        return $this->return[__FUNCTION__];
    }

    public function injectReturn($method, $return)
    {
        $this->return[$method] = $return;
    }

}
