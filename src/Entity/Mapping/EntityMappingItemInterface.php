<?php

namespace GoCatalyze\SyncCenter\Entity\Mapping;

/**
 * Item of entity-mapping, contains:
 *
 * - source (string)
 * - destination (string)
 * - callback (callable): Custom callback to convert value from source to dest.
 */
interface EntityMappingItemInterface
{

    /**
     * Set name for source attribute.
     *
     * @param string $name
     */
    public function setSource($name);

    /**
     * Get name of source attribute.
     *
     * @return string
     */
    public function getSource();

    /**
     * Set name for destination attribute.
     *
     * @param string $name
     */
    public function setDestination($name);

    /**
     * Get name of destination attribute.
     *
     * @return string
     */
    public function getDestination();

    /**
     * Set callback.
     *
     * @param callable $callable
     */
    public function setCallback($callable);

    /**
     * Get callback.
     *
     * @return null|callable
     */
    public function getCallback();
}
