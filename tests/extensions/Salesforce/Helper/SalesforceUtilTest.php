<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Salesforce\Helper;

use GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil;
use GoCatalyze\SyncCenter\Extensions\Testing\Salesforce\SalesforceTestCase;
use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;

/**
 * @group SalesforceUtil
 */
class SalesforceUtilTest extends SalesforceTestCase
{

    public function getSFUtil()
    {
        return new SalesforceUtil('http://127.0.0.1/', []);
    }

    public function testInit()
    {
        $u = $this->getSFUtil();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\SalesforceUtil', $u);
        $this->assertEquals('30.0', $u->getVersion());
    }

    /**
     * @dataProvider sourceRefreshToken
     */
    public function testRefreshToken($auth_type)
    {
        $u = $this->getSFUtil();

        if ($auth_type === 'refresh_token') {
            $u->setConfig(['refresh_token' => 'SAMPLE REFRESH TOKEN']);
        }

        // Mock response
        $auth_client = new \Guzzle\Http\Client();
        $mock = new MockPlugin();
        $mock->addResponse(new Response(200, [], '{ "access_token": "SAMPLE TOKEN", "instance_url": "http://ap2.salesforce.com/" }'));
        $auth_client->addSubscriber($mock);

        // Action
        $u->refreshToken($auth_client, true);

        // Checking
        $this->assertEquals('SAMPLE TOKEN', $u->getConfig('access_token'));
        $this->assertEquals('http://ap2.salesforce.com/', $u->getConfig('instance_url'));
    }

    public function sourceRefreshToken()
    {
        return [
            ['refresh_token'],
            ['user_password']
        ];
    }

    public function testGetEntitiesList()
    {
        $u = $this->getSFUtil();
        $u->setConfig(['access_token' => 'SAMPLE ACCESS TOKEN']);

        // mocking
        $mock = new MockPlugin();
        $mock->addResponse(new Response(200, [], ' { "sobjects": [ '
            . '{ "name": "Contact", "createable": true, "queryable": true, "searchable": true, "triggerable": true, "deprecatedAndHidden": false, "updateable": true, "layoutable": true },'
            . '{ "name": "User", "createable": true, "queryable": true, "searchable": true, "triggerable": true, "deprecatedAndHidden": false, "updateable": true, "layoutable": true }'
            . ']} '
        ));
        $u->addSubscriber($mock);

        // Action
        $response = $u->getObjectTypes();

        // Checking
        $this->assertEquals(['Contact', 'User'], $response);
    }

    public function testObjectTypeInfo()
    {
        $u = $this->getSFUtil();
        $u->setConfig([
            'access_token' => 'SAMPLE ACCESS TOKEN',
            'entity_info'  => [
                'Contact' => [
                    'fields' => ['name' => 'fake', 'foo' => 'FOO']
                ]
            ]
        ]);

        // Action
        $response = $u->getObjectTypeInfo('Contact');

        // Checking
        $this->assertEquals('fake', $response['fields']['name']);
    }

    public function testGetFields()
    {
        $u = $this->getSFUtil();
        $u->setConfig([
            'access_token' => 'SAMPLE ACCESS TOKEN',
            'entity_info'  => [
                'FuObject' => [
                    'fields' => [
                        'foo' => ['name' => 'foo'],
                        'bar' => ['name' => 'bar'],
                    ]
                ]
            ]
        ]);

        // Action
        $response = $u->getFields('FuObject');

        // Checking
        $this->assertEquals(['foo', 'bar'], $response);
    }

    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Not support converting reference field of FuObject
     */
    public function testGetReferenceFieldConvertor()
    {
        $u = $this->getSFUtil();

        $expected = 'GoCatalyze\SyncCenter\Extensions\Salesforce\Helper\ReferenceFieldConvertor\ContactReferenceConvertor';
        $this->assertInstanceOf($expected, $u->getReferenceFieldConvertor('Contact'));

        // Raise exception
        $u->getReferenceFieldConvertor('FuObject');
    }

    public function testDoCreate()
    {
        $u = $this->getSFUtil();
        $u->setConfig(['access_token' => 'SAMPLE ACCESS TOKEN', 'entity_info' => ['Contact' => ['fields' => []]]]);
        $entity = $this->dummyEntityContact();
        $full_entity = $this->dummyFullEntiytContact();

        // Mock response
        $mock = new MockPlugin();
        $mock->addResponse(new Response(200, [], ' { "fields": [ '
            . '     { "name": "Name" },  '
            . '     { "name": "Mail" } '
            . '] } '));
        $mock->addResponse(new Response(200, [], json_encode($full_entity->getAttributes())));
        $u->addSubscriber($mock);

        // Action
        $u->doCreate('Contact', $entity->getAttributes());

        // Checking
        $expected = $full_entity->getAttributeValue('Id');
    }

}
