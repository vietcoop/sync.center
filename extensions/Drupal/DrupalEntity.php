<?php

namespace GoCatalyze\SyncCenter\Extensions\Drupal;

use GoCatalyze\SyncCenter\Entity\BaseEntity;
use RuntimeException;

/**
 * Generic entity of drupal.
 */
class DrupalEntity extends BaseEntity
{

    /** @var string */
    protected $entity_name = 'drupal.entity';

    /**
     * {@inheritdoc}
     * @param string $attr_name
     * @param mixed $attr_value
     */
    public function setAttributeValue($attr_name, $attr_value)
    {
        $call_parent = true;

        if (is_array($attr_value) && isset($attr_value['und'][0])) {
            if (is_array($attr_value) && isset($attr_value['und'][0])) {
                $attr_value = $attr_value['und'][0];
            }
        }

        // Convert profile2[main]/field_name => …
        // to @references => [  profile2 => [  main => [  field_name => …  ]  ]   ]
        if (FALSE !== strpos($attr_name, '[') && FALSE !== strpos($attr_name, ']') && FALSE !== strpos($attr_name, '/')) {
            $matches = [];
            // Try to match string like `profile2[main]/field_name`
            if (preg_match('/^(\w+)\[(\w+)\]\/(\w+)$/', $attr_name, $matches)) {
                list($entity_type, $bundle, $field_name) = [$matches[1], $matches[2], $matches[3]];
                $this->attributes['@references'][$entity_type][$bundle][$field_name] = $attr_value;
                $call_parent = false;
            }
            else {
                throw new RuntimeException(sprintf('Can not set referenced value: %s', $attr_name));
            }
        }

        if ($call_parent) {
            parent::setAttributeValue($attr_name, $attr_value);
        }
    }

    public function offsetExists($offset)
    {
        $matches = [];
        if (preg_match('/^(\w+)\[(\w+)\]\/(\w+)$/', $offset, $matches)) {
            list($type, $bundle, $field_name) = [$matches[1], $matches[2], $matches[3]];
            return isset($this->attributes['@references'][$type][$bundle][$field_name]);
        }

        return parent::offsetExists($offset);
    }

    /**
     * {@inheritdoc}
     * @param string $attr_name
     */
    public function getAttributeValue($attr_name)
    {
        $return = null;

        $matches = [];
        if (preg_match('/^(\w+)\[(\w+)\]\/(\w+)$/', $attr_name, $matches)) {
            list($entity_type, $bundle, $field_name) = [$matches[1], $matches[2], $matches[3]];
            if (isset($this->attributes['@references'][$entity_type][$bundle][$field_name])) {
                return $this->attributes['@references'][$entity_type][$bundle][$field_name];
            }
        }
        elseif (isset($this->attributes[$attr_name])) {
            $return = $this->attributes[$attr_name];
        }

        if (is_array($return)) {
            if (isset($return['value'])) {
                $return = $return['value'];
            }
            elseif (empty($return)) {
                $return = null;
            }
        }

        return $return;
    }

}
