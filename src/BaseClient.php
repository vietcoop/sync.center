<?php

namespace GoCatalyze\SyncCenter;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use Guzzle\Http\Client;
use RuntimeException;

class BaseClient extends Client implements ClientInterface
{

    /**
     * @codeCoverageIgnore
     */
    public function doCreate(EntityInterface $entity, $type)
    {
        throw new RuntimeException('Unimplemented method: doCreate.');
    }

    /**
     * @codeCoverageIgnore
     */
    public function doLoad($id, $type, array $extra_fields = [])
    {
        throw new RuntimeException('Unimplemented method: doLoad.');
    }

    /**
     * @codeCoverageIgnore
     */
    public function doDelete($id)
    {
        throw new RuntimeException('Unimplemented method: doDelete.');
    }

    /**
     * @codeCoverageIgnore
     */
    public function doUpdate(EntityInterface $entity, $id, $type)
    {
        throw new RuntimeException('Unimplemented method: doUpdate.');
    }

    /**
     * @codeCoverageIgnore
     */
    public function doQuery(EntityQueryBuilderInterface $query, $type)
    {
        throw new RuntimeException('Unimplemented method: doQuery.');
    }

}
