<?php

use Composer\Autoload\ClassLoader;

// ---------------------
// Auto class loading
// ---------------------
$locations[] = __DIR__ . "/../vendor/autoload.php";
$locations[] = __DIR__ . "/../../../autoload.php";

foreach ($locations as $location) {
    if (is_file($location)) {
        /* @var $loader ClassLoader */
        $loader = require $location;
        $loader->addPsr4('GoCatalyze\\SyncCenter\\Extensions\\', dirname(__DIR__) . '/extensions');
        $loader->addPsr4('GoCatalyze\\SyncCenter\\Testing\\',             __DIR__ . '/interfaces');
        $loader->addPsr4('GoCatalyze\\SyncCenter\\Extensions\\Testing\\', __DIR__ . '/extensions');
        $loader->addPsr4('GoCatalyze\\SyncCenter\\Testing\\Fixtures\\',   __DIR__ . '/fixtures');
        break;
    }
}
