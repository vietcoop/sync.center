<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Drupal;

use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalService;
use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;

/**
 * @group DrupalService
 */
class DrupalServiceTest extends DrupalTestCase
{

    private function getDefaultConfig()
    {
        return ['host' => 'http://127.0.0.1/', 'user' => 'admin', 'password' => 'password'];
    }

    /**
     *
     * @param boolean $auth
     * @param array $mock_responses
     * @return DrupalService
     */
    private function getService($auth = false, $mock_responses = [])
    {
        $service = new DrupalService();
        $service->setConfiguration($this->getDefaultConfig());

        if ($auth) {
            $client = new DrupalClient();

            // Mock subscriber
            $mock_subscriber = new MockPlugin();
            // Get token
            $mock_subscriber->addResponse(new Response(200, [], '{ "token": "SOME TOKEN" }'));
            // Entity info
            $mock_subscriber->addResponse(new Response(200, [], '{ }'));
            // Field info
            $mock_subscriber->addResponse(new Response(200, [], '{ }'));

            // Inject from arguments
            foreach ($mock_responses as $mock_response) {
                $mock_subscriber->addResponse($mock_response);
            }

            $client->addSubscriber($mock_subscriber);
            $service->setClient($client);
        }

        return $service;
    }

    public function testInitService()
    {
        $service = $this->getService();
        $this->assertEquals('drupal', $service->getName());
    }

    /**
     * @expectedException \Guzzle\Http\Exception\CurlException
     * @expectedExceptionMessage Failed connect to 127.0.0.1:80; Connection refused [url] http://127.0.0.1/system/entity_info
     */
    public function testGetDefaultClient()
    {
        $service = $this->getService();
        $this->assertEquals('GoCatalyze\SyncCenter\ClientInterface', $service->getClient());
    }

    public function testQuery()
    {
        $service = $this->getService();
        $config = $service->getConfiguration() + ['is_views_resource' => true];
        $service->setConfiguration($config);

        // Mock and inject client/query
        $client = $this->getMock('GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient');
        $client->expects($this->once())
            ->method('doQuery')
            ->with(
                $this->equalTo($query = $this->getMock('GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntityQuery')), $this->equalTo('node')
            )
        ;
        $service->setClient($client);

        // Call method, mock should work as expected
        $service->query($query, 'node');
    }

    public function testMergeUpdate()
    {
        $service = $this->getService(true);
        $service->setConfiguration([
            'entity_info' => [
                'node' => [
                    'label'       => 'Node',
                    'entity keys' => ['id' => 'nid'],
                    'fields'      => ['article' => ['body' => ['label' => 'Body', 'widget' => ['type' => 'text_textarea_with_summary']]]]
                ]
            ]
        ]);

        // Dummy data
        $entity_input = ['nid' => 123, 'type' => 'article', 'title' => 'Demo node title', 'path' => 'demo-node-title'];
        $entity = $this->getManager()->entityFromArray($entity_input, 'drupal.entity', 'node');
        $mapping = \GoCatalyze\SyncCenter\Entity\Mapping\EntityMapping::fromArray([
                ['source' => 'drupal_entity_id__c', 'destination' => 'nid'],
                ['source' => 'title__c', 'destination' => 'title'],
                ['source' => 'slug__c', 'destination' => 'path']
        ]);

        // Mock to tell remote ref.entity finder always return null, inject it
        // to service
        $finder = $this->getMockBuilder('GoCatalyze\SyncCenter\Extensions\Drupal\Helper\EntityFullLoader')
            ->disableOriginalConstructor()
            ->getMock();
        $finder->expects($this->once())
            ->method('load')
            ->with($this->equalTo($entity), $this->equalTo('node'), $this->equalTo($mapping), $this->equalTo(['path']))
            ->willReturn($entity)
        ;
        $service->setEntityFullLoader($finder);

        // Mock client, expect 'doCreate' method is executed
        $client = $this->getMock('GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient');
        $client->expects($this->once())
            ->method('doUpdate')
            ->with($this->equalTo($entity), 123, $this->equalTo('node'))
        ;
        $service->setClient($client);

        // Action
        $service->merge($entity, 'node', $mapping, ['path']);
    }

    public function testMergeCreate()
    {
        $service = $this->getService(true);

        // Dummy data
        $entity_input = ['type' => 'article', 'title' => 'Demo node title', 'path' => 'demo-node-title'];
        $entity = $this->getManager()->entityFromArray($entity_input, 'drupal.entity', 'node');

        $mapping = \GoCatalyze\SyncCenter\Entity\Mapping\EntityMapping::fromArray([
                ['source' => 'drupal_entity_id__c', 'destination' => 'nid'],
                ['source' => 'title__c', 'destination' => 'title'],
                ['source' => 'slug__c', 'destination' => 'path']
        ]);

        // Mock to tell remote ref.entity finder always return null, inject it
        // to service
        $finder = $this->getMockBuilder('GoCatalyze\SyncCenter\Extensions\Drupal\Helper\EntityFullLoader')
            ->disableOriginalConstructor()
            ->getMock();

        $finder->expects($this->once())
            ->method('load')
            ->with($this->equalTo($entity), $this->equalTo('node'), $this->equalTo($mapping), $this->equalTo(['path']))
            ->willReturn(null)
        ;
        $service->setEntityFullLoader($finder);

        // Mock client, expect 'doCreate' method is executed
        $client = $this->getMock('GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient');
        $client->expects($this->once())
            ->method('doCreate')
            ->with($this->equalTo($entity), $this->equalTo('node'))
        ;
        $service->setClient($client);

        // Action
        $service->merge($entity, 'node', $mapping, ['path']);
    }

    public function testLoadRemoteEntity()
    {
        $service = $this->getService();

        // Dummy data
        $entity_input = ['type' => 'article', 'title' => 'Demo node title'];
        $entity = $this->getManager()->entityFromArray($entity_input, 'drupal.entity', 'node');
        $mapping = \GoCatalyze\SyncCenter\Entity\Mapping\EntityMapping::fromArray([
                ['source' => 'drupal_entity_id__c', 'destination' => 'nid'],
                ['source' => 'title__c', 'destination' => 'title'],
        ]);

        // Mock and inject remote entity finder
        $remote_entity_finder = $this->getMockBuilder('GoCatalyze\SyncCenter\Extensions\Drupal\Helper\EntityFullLoader')
            ->disableOriginalConstructor()
            ->getMock();

        $remote_entity_finder
            ->expects($this->once())
            ->method('load')
            ->with($this->equalTo($entity), $this->equalTo('node'), $this->equalTo($mapping), $this->equalTo(['path']))
        ;

        $service->setEntityFullLoader($remote_entity_finder);

        // Action
        $service->loadFull($entity, 'node', $mapping, ['path']);
    }

    public function testDefaultReferencedEntityFullLoader()
    {
        $service = $this->getService();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Extensions\Drupal\Helper\EntityFullLoader', $service->getEntityFullLoader());
    }

    /**
     * @dataProvider sourceGetRemoteEntityInfo
     */
    public function testGetRemoteEntityInfo($e_info, $f_info)
    {
        // Mock responses: fetch token, entity info, field info
        $mock_subscriber = new MockPlugin();
        # $mock_subscriber->addResponse(new Response(200, [], '{ "token": "SOME TOKEN" }'));
        $mock_subscriber->addResponse(new Response(200, [], json_encode($e_info)));
        $mock_subscriber->addResponse(new Response(200, [], json_encode($f_info)));

        /* @var $client DrupalClient */
        $service = $this->getService();
        $client = new DrupalClient();
        $client->addSubscriber($mock_subscriber);
        $service->setClient($client);

        // Action
        // -------
        $info = $service->getRemoteEntityInfo();

        // Check results
        $this->assertEquals('nid', $info['node']['entity keys']['id']);
        $this->assertEquals('uid', $info['user']['entity keys']['id']);
        $this->assertEquals('text_textarea_with_summary', $info['node']['fields']['body']['widget']['type']);
        $this->assertEquals('taxonomy_autocomplete', $info['node']['fields']['field_tags']['widget']['type']);

        // Call again with param, result should be static cached, no more need
        // mock more responses.
        // -------
        $node_info = $service->getRemoteEntityInfo('node');
        $this->assertEquals('nid', $info['node']['entity keys']['id']);
        $this->assertEquals('text_textarea_with_summary', $info['node']['fields']['body']['widget']['type']);
        $this->assertEquals('taxonomy_autocomplete', $info['node']['fields']['field_tags']['widget']['type']);

        // Call again with invalid param
        // -------
        $this->assertNull($service->getRemoteEntityInfo('invalid_entity_type'));
    }

    public function sourceGetRemoteEntityInfo()
    {
        $e_info[0]['node'] = ['label' => 'Node', 'entity keys' => ['id' => 'nid']];
        $e_info[0]['user'] = ['label' => 'User', 'entity keys' => ['id' => 'uid']];

        $f_info[0]['node']['page']['body'] = ['label' => 'Body', 'widget' => ['type' => 'text_textarea_with_summary']];
        $f_info[0]['node']['article']['body'] = ['label' => 'Body', 'widget' => ['type' => 'text_textarea_with_summary']];
        $f_info[0]['node']['article']['field_tags'] = ['label' => 'Tags', 'widget' => ['type' => 'taxonomy_autocomplete']];

        return [
            [$e_info[0], $f_info[0]]
        ];
    }

    /**
     * @dataProvider sourceGetRemoteEntityInfo
     */
    public function testDrupalize($e_info, $f_info)
    {
        // Mock responses: fetch token, entity info, field info
        $mock_subscriber = new MockPlugin();
        $mock_subscriber->addResponse(new Response(200, [], json_encode($e_info)));
        $mock_subscriber->addResponse(new Response(200, [], json_encode($f_info)));

        /* @var $client DrupalClient */
        $service = $this->getService();
        $client = new DrupalClient();
        $client->addSubscriber($mock_subscriber);
        $service->setClient($client);

        $input = ['nid' => 111, 'title' => 'Node title #111', 'body' => ['value' => 'Node body']];
        $entity = $this->getManager()->entityFromArray($input, 'drupal.entity', 'node');

        $expected = $input;
        $expected['body'] = ['und' => [0 => 'Node body']]; # @TODO: body.und.0.value
        $this->assertEquals($expected, $service->drupalize($entity, 'node'));
    }

}
