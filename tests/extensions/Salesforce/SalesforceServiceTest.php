<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Salesforce;

use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceService;

/**
 * @group SalesforceService
 */
class SalesforceServiceTest extends SalesforceTestCase
{

    public function getService()
    {
        return new SalesforceService();
    }

    public function testInit()
    {
        $service = $this->getService();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\ServiceInterface', $service);
        $this->assertEquals('salesforce', $service->getName());
    }

    public function testGetDefaultClient()
    {
        $service = $this->getService();
        $this->assertInstanceOf('\GoCatalyze\SyncCenter\ClientInterface', $service->getClient());
    }

    public function testMergeUpdate()
    {
        $service = $this->getService();
        $entity = $this->dummyEntityContact();
        $full_entity = $this->dummyFullEntiytContact();
        $mapping = $this->dummyMapping(false);
        $uniques = $this->dummyUniques();
        $client = $this->getMock('GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceClient');
        $client->expects($this->once())
            ->method('doQuery')
            ->with($this->anything(), $this->equalTo('Contact'))
            ->willReturn([$full_entity])
        ;

        $client->expects($this->once())
            ->method('doUpdate')
            ->with($full_entity, 112233, 'Contact')
            ->willReturn($full_entity)
        ;

        $service->setClient($client);
        $response = $service->merge($entity, 'Contact', $mapping, $uniques);

        $this->assertEquals(112233, $response->getAttributeValue('Id'));
    }

    public function testMergeCreate()
    {
        $service = $this->getService();
        $entity = $this->dummyEntityContact();
        $full_entity = $this->dummyFullEntiytContact();
        $mapping = $this->dummyMapping(false);
        $uniques = $this->dummyUniques();
        $client = $this->getMock('GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceClient');
        $client->expects($this->once())
            ->method('doQuery')
            ->with($this->anything(), 'Contact')
            ->willReturn(false)
        ;

        $client->expects($this->once())
            ->method('doCreate')
            ->with($entity, 'Contact')
            ->willReturn($full_entity)
        ;

        $service->setClient($client);
        $response = $service->merge($entity, 'Contact', $mapping, $uniques);

        $this->assertEquals(112233, $response->getAttributeValue('Id'));
    }

}
