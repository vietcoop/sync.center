<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures\Entity;

use GoCatalyze\SyncCenter\Entity\BaseEntity;

class DestEntity extends BaseEntity
{

    protected $entity_name = 'demo.destination';

    /**
     * Attribute info.
     *
     * @var array
     */
    protected $rules = [
        'bar' => ['required' => true, 'type' => 'mixed'],
        'dest_string' => ['type' => 'string'],
        'dest_datetime' => [
            'type' => 'GoCatalyze\\SyncCenter\\Testing\\Fixtures\\Entity\Attributes\\DateTime',
        ],
    ];

}
