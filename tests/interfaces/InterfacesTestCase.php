<?php

namespace GoCatalyze\SyncCenter\Testing;

use GoCatalyze\SyncCenter\Testing\Fixtures\DemoManager;
use PHPUnit_Framework_TestCase;

abstract class InterfacesTestCase extends PHPUnit_Framework_TestCase
{

    /** @var DemoManager */
    protected $manager;

    public function getManager()
    {
        if (null === $this->manager) {
            $this->manager = new DemoManager();
        }
        return $this->manager;
    }

}
