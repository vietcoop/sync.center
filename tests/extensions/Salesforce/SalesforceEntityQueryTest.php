<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Salesforce;

use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntityQuery;
use GoCatalyze\SyncCenter\Extensions\Testing\Salesforce\SalesforceTestCase;

/**
 * @group SalesforceEntityQuery
 */
class SalesforceEntityQueryTest extends SalesforceTestCase
{

    public function getEntityQuery()
    {
        return new SalesforceEntityQuery();
    }

    public function testInit()
    {
        $query = $this->getEntityQuery();
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntityQuery', $query);
    }

    public function testBuildQuery()
    {
        $datetime = new \DateTime('- 1 hour');

        $query = $this->getEntityQuery();
        $query->fields(['Id', 'Name', 'Mail']);
        $query->queryLatestEntities($datetime, 'Contact');
        $query->limit(10);
        $query->offset(10);

        $expected = 'SELECT Id,Name,Mail FROM Contact';
        $expected .= ' WHERE LastModifiedDate>%datetime';
        $expected .= ' ORDER BY LastModifiedDate DESC LIMIT 10 OFFSET 10';

        $dt = str_replace('+', '%2B', $datetime->format(DATE_ISO8601));
        $this->assertEquals(str_replace('%datetime', $dt, $expected), (string) $query);
    }

    public function testBuildQueryCondition()
    {
        $datetime = new \DateTime('- 1 hour');

        $query = $this->getEntityQuery();
        $query->setRemoteEntityType('Contact');
        $query->fields(['Id', 'Name', 'Mail']);
        $query->condition('Id', 111, '<');
        $query->condition('Id', [11, 22, 33], '=');
        $query->limit(10);
        $query->offset(10);

        $expected = 'SELECT Id,Name,Mail FROM Contact';
        $expected .= " WHERE Id<111 AND Id IN ('11','22','33')";
        $expected .= ' LIMIT 10 OFFSET 10';
        $dt = str_replace('+', '%2B', $datetime->format(DATE_ISO8601));
        $this->assertEquals(str_replace('%datetime', $dt, $expected), (string) $query);
    }

}
