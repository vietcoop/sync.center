<?php

namespace GoCatalyze\SyncCenter\Extensions\Drupal;

use GoCatalyze\SyncCenter\Entity\Query\BaseEntityQuery;

class DrupalEntityQuery extends BaseEntityQuery
{

    /** @var string */
    protected $entity_query_name = 'drupal.entity';

    /** @var boolean Views resource has different query string */
    private $is_views_resource = false;

    /** @var string[] Name of query field for entity.updated */
    protected $query_field_updated_at = [
        'user'          => 'created',
        'file'          => 'created',
        'taxonomy_term' => 'tid',
        'role'          => 'rid',
        'default'       => 'changed'
    ];

    /** @var string Entity endpoint */
    private $endpoint;

    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function setIsViewsResource($is_views_resource)
    {
        $this->is_views_resource = $is_views_resource;
    }

    /**
     * {@inheritdoc}
     * @param int|DateTime $since
     * @param string $type
     */
    public function queryLatestEntities($since, $type)
    {
        if (null !== $type) {
            $this->setRemoteEntityType($type);
        }

        if (!isset($this->query_field_updated_at[$type])) {
            $this->query_field_updated_at[$type] = 'changed';
            // throw new \RuntimeException(sprintf('Do not know sort column for %s', $type));
        }

        $column = $this->query_field_updated_at[$type];

        $this->sort($column, 'ASC');

        // services_entity only supports timestamp for now
        $timestamp = is_int($since) ? $since : $since->getTimestamp();
        $this->condition($column, $timestamp, '>');
    }

    /**
     * Magic method to represent object as string.
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->is_views_resource) {
            return $this->__toStringViewsResource();
        }
        return $this->__toStringServicesResource();
    }

    /**
     * Details for __toString().
     *
     * @return string
     */
    private function __toStringViewsResource()
    {
        $return = [];

        foreach ($this->condition_groups['default']['expressions'] as $cond) {
            $return[] = "{$cond[0]}={$cond[1]}";
        }

        foreach ($this->sorts as $k => $v) {
            $return[] = "sort_by={$k}";
            $return[] = "sort_order={$v}";
        }

        $endpoint = !is_null($this->endpoint) ? $this->endpoint : "{$this->remote_entity_type}.json";
        return $endpoint . '?' . implode('&', $return);
    }

    /**
     * Details for __toString().
     *
     * @see https://www.drupal.org/project/services_entity
     * @return string e.g. entity/node.json?fields=nid,uid,title,changed&parameters[status]=1
     */
    private function __toStringServicesResource()
    {
        $params = [];
        if (!empty($this->fields)) {
            $params[] = 'fields=' . implode(',', $this->fields);
        }

        // rest_server doesn't support group of condition.
        foreach ($this->condition_groups['default']['expressions'] as $cond) {
            if ('=' === $cond[2]) {
                // Build parameters part — e.g. &parameters[status]=1&parameters[uid]=2
                $params[] = 'parameters[' . $cond[0] . ']' . $cond[2] . $cond[1];
            }
            else {
                // &parameters[status][operation]=1parameters[status][value]=1
                $params[] = "parameters[{$cond[0]}][operation]={$cond[2]}&parameters[{$cond[0]}][value]={$cond[1]}";
            }
        }

        // sort
        foreach ($this->sorts as $sort_field => $sort_direction) {
            $params[] = "sort={$sort_field}&direction={$sort_direction}";

            // only support one sort credential
            break;
        }

        $endpoint = "{$this->remote_entity_type}.json";
        if (!is_null($this->endpoint)) {
            $endpoint = $this->endpoint;
        }

        return $endpoint . '?' . implode('&', $params);
    }

}
