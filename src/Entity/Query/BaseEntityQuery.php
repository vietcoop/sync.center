<?php

namespace GoCatalyze\SyncCenter\Entity\Query;

use DateTime;
use RuntimeException;

abstract class BaseEntityQuery implements EntityQueryBuilderInterface
{

    /** @var string */
    protected $entity_query_name;

    /** @var string Remote entity type */
    protected $remote_entity_type = 'node';

    /** @var string[] Name of query field for entity.updated */
    protected $query_field_updated_at = [];

    /** @var array Condition groups */
    protected $condition_groups = [
        'default' => [
            'type'        => 'and',
            'expressions' => [],
        ]
    ];

    /** @var array The list of sorts. */
    protected $sorts = [];

    /** @var array List of selected fields */
    public $fields = [];

    /** @var int */
    protected $limit;

    /** @var int */
    protected $offset = 0;

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getEntityTypeName()
    {
        return $this->entity_query_name;
    }

    /**
     * {@inheritdoc}
     * @param string $type
     */
    public function setRemoteEntityType($type)
    {
        $this->remote_entity_type = $type;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getRemoteEntityType()
    {
        return $this->remote_entity_type;
    }

    /**
     * {@inheritdoc}
     * @return string[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getSorts()
    {
        return $this->sorts;
    }

    /**
     * Set query_field_updated_at.
     *
     * @return string
     */
    public function getQueryFieldUpdatedAt($remote_entity_type)
    {
        if (isset($this->query_field_updated_at[$remote_entity_type])) {
            return $this->query_field_updated_at[$remote_entity_type];
        }

        if (isset($this->query_field_updated_at['default'])) {
            return $this->query_field_updated_at['default'];
        }
    }

    /**
     * Set query_field_updated_at.
     *
     * @param string $remote_entity_type
     * @param string $query_field_updated_at
     */
    public function setQueryFieldUpdatedAt($remote_entity_type, $query_field_updated_at)
    {
        $this->query_field_updated_at[$remote_entity_type] = $query_field_updated_at;
    }

    /**
     * {@inheritdoc}
     * @param string $field
     * @param mixed $value
     * @param string $op
     * @return BaseEntityQuery
     */
    public function condition($field, $value, $op = '=')
    {
        $this->condition_groups['default']['expressions'][] = [$field, $value, $op];
        return $this;
    }

    /**
     * (No extension needs this for now)
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @param string $group_name
     * @param string $field
     * @param mixed $value
     * @param string $op
     * @return EntityQueryBuilderInterface
     * @throws \RuntimeException
     */
    public function andCondition($group_name, $field, $value, $op = '')
    {
        if (!isset($this->condition_groups[$group_name])) {
            $this->condition_groups[$group_name] = [
                'type' => 'and'
            ];
        }
        elseif ('and' !== $this->condition_groups[$group_name]['type']) {
            throw new RuntimeException(sprintf('Can not add new and-expression to or-group: %s', $group_name));
        }

        $this->condition_groups[$group_name]['expressions'][] = [$field, $value, $op];
        return $this;
    }

    /**
     * (No extension needs this for now)
     * {@inheritdoc}
     * @codeCoverageIgnore
     * @param string $group_name
     * @param string $field
     * @param mixed $value
     * @param string $op
     * @return EntityQueryBuilderInterface
     * @throws \RuntimeException
     */
    public function orCondition($group_name, $field, $value, $op = '')
    {
        if (!isset($this->condition_groups[$group_name])) {
            $this->condition_groups[$group_name] = [
                'type' => 'or'
            ];
        }
        elseif ('or' !== $this->condition_groups[$group_name]['type']) {
            throw new RuntimeException(sprintf('Can not add new or-expression to and-group: %s', $group_name));
        }

        $this->condition_groups[$group_name]['expressions'][] = [$field, $value, $op];
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getConditionGroups()
    {
        return $this->condition_groups;
    }

    /**
     * {@inheritdoc}
     * @param string $field
     * @param string $direction ASC or DESC
     * @return EntityQueryBuilderInterface
     */
    public function sort($field, $direction = 'ASC')
    {
        $this->sorts[$field] = $direction;
        return $this;
    }

    /**
     * {@inheritdoc}
     * @param string[] $fields
     * @return EntityQueryBuilderInterface
     */
    public function fields($fields)
    {
        $this->fields = array_merge($this->fields, $fields);
        return $this;
    }

    /**
     * Limit the result.
     *
     * @param int $limit
     */
    public function limit($limit)
    {
        $this->limit = (int) $limit;
    }

    public function offset($offset)
    {
        $this->offset = (int) $offset;
    }

    /**
     * {@inheritdoc}
     * @param int|DateTime $since
     * @param string $type
     */
    public function queryLatestEntities($since, $type)
    {
        if ($column = $this->getQueryFieldUpdatedAt($type)) {
            $since = is_int($since) ? $since : $since->format(\DATE_ISO8601);
            $this->condition($column, $since, '>');
            $this->sort($column, 'ASC');
        }
    }

}
