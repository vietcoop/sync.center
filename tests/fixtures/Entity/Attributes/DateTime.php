<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures\Entity\Attributes;

use GoCatalyze\SyncCenter\Entity\EntityAttributeValidatorInterface;

class DateTime implements EntityAttributeValidatorInterface
{
    public function validate($input)
    {
        return is_object($input) && ($input instanceof \DateTime);
    }

}
