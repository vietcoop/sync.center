<?php

namespace GoCatalyze\SyncCenter\Entity\Mapping;

use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertor\AttributeMappingConvertor;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertor\DestinationEntityIdDetector;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertorInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use GoCatalyze\SyncCenter\ManagerInterface;
use GoCatalyze\SyncCenter\ServiceInterface;

abstract class EntityConvertorBase implements EntityConvertorInterface
{

    /** @var ManagerInterface Sync Center manager. */
    protected $manager;

    /** @var ServiceInterface */
    protected $source_service;

    /** @var string */
    protected $dest_entity_type;

    /** @var string */
    protected $dest_remote_entity_type;

    /** @var ServiceInterface */
    protected $destination_service;

    /** @var EntityQueryBuilderInterface */
    protected $destination_query_builder;

    /** @var array */
    protected $destination_unique_fields;

    /** @var AttributeMappingConvertor */
    protected $attribute_mapping_convertor;

    /** @var DestinationEntityIdDetector */
    protected $destination_entity_id_detector;

    /**
     * Constructor.
     *
     * @param ManagerInterface $manager
     */
    public function __construct(ManagerInterface $manager)
    {
        $this->manager = $manager;
        return $this;
    }

    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Inject source service.
     *
     * @param ServiceInterface $service
     */
    public function setSourceService(ServiceInterface $service)
    {
        $this->source_service = $service;
        return $this;
    }

    /**
     * Get source service.
     *
     * @return ServiceInterface
     */
    public function getSourceService()
    {
        return $this->source_service;
    }

    /**
     * Inject destination service.
     *
     * @param ServiceInterface $service
     */
    public function setDestinationService(ServiceInterface $service)
    {
        $this->destination_service = $service;
        return $this;
    }

    /**
     * Get destination service.
     *
     * @return ServiceInterface
     */
    public function getDestinationService()
    {
        return $this->destination_service;
    }

    public function setDestinationType($type)
    {
        $this->dest_entity_type = $type;
        return $this;
    }

    public function setDestinationRemoteType($type)
    {
        $this->dest_remote_entity_type = $type;
        return $this;
    }

    public function getDestinationRemoteType()
    {
        return $this->dest_remote_entity_type;
    }

    public function setDestinationUniqueFields(array $d_uniques)
    {
        $this->destination_unique_fields = $d_uniques;
        return $this;
    }

    public function setDestinationEntityQueryBuilder(EntityQueryBuilderInterface $query)
    {
        $this->destination_query_builder = $query;
        return $this;
    }

    public function getDestinationEntityQueryBuilder()
    {
        return $this->destination_query_builder;
    }

    public function getDestinationType()
    {
        return $this->dest_entity_type;
    }

    public function getAttributeMappingConvertor()
    {
        if (null === $this->attribute_mapping_convertor) {
            $this->attribute_mapping_convertor = new AttributeMappingConvertor($this);
        }
        return $this->attribute_mapping_convertor;
    }

    public function getDestinationEntityIdDetector()
    {
        if (null === $this->destination_entity_id_detector) {
            $this->destination_entity_id_detector = new DestinationEntityIdDetector($this);
        }
        return $this->destination_entity_id_detector;
    }

}
