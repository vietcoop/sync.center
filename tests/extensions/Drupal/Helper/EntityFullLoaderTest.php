<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Drupal\Helper;

use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalService;
use GoCatalyze\SyncCenter\Extensions\Drupal\Helper\EntityFullLoader;
use GoCatalyze\SyncCenter\Extensions\Testing\Drupal\DrupalTestCase;
use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;

/**
 * @group DrupalEntityFullLoader
 */
class EntityFullLoaderTest extends DrupalTestCase
{

    private function getLoader()
    {
        return new EntityFullLoader(new DrupalService());
    }

    private function getMockSubscriber($include_token = true)
    {
        $mock = new MockPlugin();

        if ($include_token) {
            $mock->addResponse(new Response(200, [], '{ "token": "SAMPLE TOKEN" }'));
        }

        return $mock;
    }

    public function testInit()
    {
        $this->assertInstanceOf('GoCatalyze\SyncCenter\Extensions\Drupal\Helper\EntityFullLoader', $this->getLoader());
    }

    /**
     * @runInSeparateProcess
     */
    public function testLoad()
    {
        $loader = $this->getLoader();
        $service = $loader->getService();
        $entity = $this->getDummyEntity();
        $mapping = $this->getDummyMapping();
        $uniques = $this->getDummyUniques();

        // Dummy service config
        $service->setConfiguration([
            'entity_info' => [
                'node' => [
                    'label'       => 'Node',
                    'entity keys' => ['id' => 'nid'],
                    'fields'      => ['article' => ['body' => ['label' => 'Body', 'widget' => ['type' => 'text_textarea_with_summary']]]]
                ]
            ]
        ]);

        // Mock responses and inject them to service's client.
        $client = new DrupalClient();
        $subscriber = $this->getMockSubscriber(true);

        // service -> load entity by ID, just return $entity
        $subscriber->addResponse(new Response(200, [], json_encode($entity->getAttributes())));

        $client->addSubscriber($subscriber);
        $service->setClient($client);

        // Action
        $response = $loader->load($entity, 'node', $mapping, $uniques);

        // Checking: Because current entity on remote is as same as what we have
        // response should be not changed
        $this->assertEquals($entity, $response);
    }

    public function testLoadWithReferences()
    {
        $loader = $this->getLoader();
        $service = $loader->getService();
        $entity = $this->getDummyEntity();
        $mapping = $this->getDummyMapping(true);
        $uniques = $this->getDummyUniques();
        $user_entity = new \GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity();
        $user_entity->setAttributeValues(['uid' => 222, 'name' => 'John English', 'mail' => 'john@english.com']);

        // Check no reference provided by default
        $this->assertTrue(!isset($entity['@references']['user']['default']));

        // Dummy service config
        $service->setConfiguration([
            'entity_info' => [
                'node' => [
                    'label'       => 'Node',
                    'entity keys' => ['id' => 'nid'],
                    'fields'      => ['article' => ['body' => ['label' => 'Body', 'widget' => ['type' => 'text_textarea_with_summary']]]]
                ]
            ]
        ]);

        // Mock responses and inject them to service's client.
        $client = $this->getMock('GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient');
        $client->expects($this->once()) // loading node
            ->method('doLoad')
            ->with($this->equalTo($entity->getAttributeValue('nid')), $this->equalTo('node'), $this->anything())
            ->willReturn($entity)
        ;
        $client->expects($this->atLeastOnce()) // loading user
            ->method('doQuery')
            ->with($this->anything(), $this->equalTo('user'))
            ->willReturn([$user_entity->getAttributes()])
        ;
        $service->setClient($client);

        // Action
        $response = $loader->load($entity, 'node', $mapping, $uniques);

        // Checking
        $this->assertTrue(isset($response['@references']['user']['default']));
    }

    public function testLoadByUniqueFields()
    {
        $loader = $this->getLoader();
        $service = $loader->getService();
        $service->setConfiguration(['entity_info' => ['node' => ['entity keys' => ['id' => 'nid']]]]);
        $uniques = $this->getDummyUniques();
        $entity = new \GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity();
        $entity->setAttributeValues(['path' => 'article/demo-article-node.html', 'title' => 'Demo article node']);
        $full_entity = $this->getDummyEntity();
        $mapping = $this->getDummyMapping();

        // Mock responses and inject them to service's client.
        $client = $this->getMock('GoCatalyze\SyncCenter\Extensions\Drupal\DrupalClient');
        $client->expects($this->any()) // loading node by ID
            ->method('doLoad')
            ->with($this->equalTo($entity->getAttributeValue('nid')), $this->equalTo('node'), $this->anything())
            ->willReturn(false)
        ;
        $client->expects($this->atLeastOnce()) // find node by unique fields
            ->method('doQuery')
            ->with($this->anything(), $this->equalTo('node'))
            ->willReturn([$full_entity])
        ;
        $service->setClient($client);

        // Action
        $response = $loader->load($entity, 'node', $mapping, $uniques);

        // Checking
        $this->assertEquals($full_entity['nid'], $response->getAttributeValue('nid'));
    }

}
