<?php

namespace GoCatalyze\SyncCenter\Extensions\Salesforce;

use GoCatalyze\SyncCenter\Entity\Query\BaseEntityQuery;

class SalesforceEntityQuery extends BaseEntityQuery
{

    /** @var string */
    protected $entity_query_name = 'salesforce.entity';

    /** @var string Name of query field for entity.updated. */
    protected $query_field_updated_at = 'LastModifiedDate';

    /**
     * Add a condition to the query.
     *
     * @param string $field
     *                         Field name.
     * @param mixed  $value
     *                         Condition value. If an array, it will be split into quote enclosed
     *                         strings separated by commas inside of parenthesis. Note that the caller
     *                         must enclose the value in quotes as needed by the SF API.
     * @param string $operator
     *                         Conditional operator. One of '=', '!=', '<', '>', 'LIKE, 'IN', 'NOT IN'.
     */
    public function condition($field, $value, $operator = '=')
    {
        if (is_array($value)) {
            $value = "('" . implode("','", $value) . "')";

            // Set operator to IN if wasn't already changed from the default.
            if ($operator == '=') {
                $operator = ' IN ';
            }
        }

        parent::condition($field, $value, $operator);
    }

    /**
     * {@inheritdoc}
     * @param \DateTime $since
     * @param string $type
     */
    public function queryLatestEntities($since, $type)
    {
        $this->sort($this->query_field_updated_at, 'DESC');
        $this->condition($this->query_field_updated_at, $since->format(DATE_ISO8601), '>');
        $this->setRemoteEntityType($type);
    }

    /**
     * Implements PHP's magic toString().
     *
     * Function to convert the query to a string to pass to the SF API.
     *
     * @return string
     *                SOQL query ready to be executed the SF API.
     */
    // @codingStandardsIgnoreStart
    public function __toString()
    {

        $query = 'SELECT ';
        $query .= implode(',', $this->fields);
        $query .= " FROM " . $this->getRemoteEntityType();

        if (count($this->condition_groups['default']['expressions']) > 0) {
            $where = [];
            foreach ($this->condition_groups['default']['expressions'] as $condition) {
                $where[] = implode('', [$condition[0], $condition[2], str_replace('+', '%2B', $condition[1])]);
            }
            $query .= ' WHERE ' . implode(' AND ', $where);
        }

        if ($this->sorts) {
            $query .= " ORDER BY ";
            $fields = [];
            foreach ($this->sorts as $field => $direction) {
                $fields[] = "$field $direction";
            }
            $query .= implode(',', $fields);
        }

        if (null !== $this->limit) {
            $query .= " LIMIT {$this->limit}";
        }

        if (null !== $this->offset) {
            $query .= " OFFSET {$this->offset}";
        }

        return $query;
    }

}
