<?php

namespace GoCatalyze\SyncCenter;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertor;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityConvertorInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use GoCatalyze\SyncCenter\ServiceInterface;
use OutOfBoundsException;
use RuntimeException;

class BaseManager implements ManagerInterface
{

    /**
     * Entity convertor object.
     *
     * @var EntityConvertorInterface
     */
    protected $entity_convertor;

    /**
     * Entity types.
     *
     * @var EntityInterface[]
     */
    protected $entity_types = [];

    /**
     * Services
     *
     * @var ServiceInterface[]
     */
    protected $services = [];

    /**
     * Entity queries.
     *
     * @var EntityQueryBuilderInterface[]
     */
    protected $entity_queries = [];

    /**
     * Extensions
     *
     * @var ExtensionInterface[]
     */
    protected $extensions = [];

    /**
     * {@inheritdoc}
     * @param EntityConvertorInterface $convertor
     */
    public function setEntityConvertor(EntityConvertorInterface $convertor)
    {
        $this->entity_convertor = $convertor;
    }

    /**
     * {@inheritdoc}
     * @return EntityConvertorInterface
     */
    public function getEntityConvertor()
    {
        if (is_null($this->entity_convertor)) {
            $convertor = new EntityConvertor($this);
            $this->setEntityConvertor($convertor);
        }
        return $this->entity_convertor;
    }

    /**
     * {@inheritdoc}
     * @param string $name
     * @return EntityInterface
     */
    public function getEntityType($name)
    {
        if (!isset($this->entity_types[$name])) {
            throw new OutOfBoundsException(\sprintf('Content type is not registered: %s', $name));
        }

        return clone $this->entity_types[$name];
    }

    /**
     * {@inheritdoc}
     * @param string $name
     * @return ExtensionInterface
     */
    public function getExtension($name)
    {
        return $this->extensions[$name];
    }

    /**
     * {@inheritdoc}
     * @return ExtensionInterface[]
     */
    public function getExtensions()
    {
        return $this->extensions;
    }

    /**
     * {@inheritdoc}
     * @return string[]
     */
    public function getExtensionNames()
    {
        return array_keys($this->extensions);
    }

    /**
     * {@inheritdoc}
     * @param string $name
     * @return ServiceInterface
     */
    public function getService($name, array $config = [])
    {
        if (!isset($this->services[$name])) {
            throw new OutOfBoundsException(sprintf('Service is not registered: %s', $name));
        }

        // Set options
        if (!empty($config)) {
            $this->services[$name]->setConfiguration($config);
        }

        return $this->services[$name];
    }

    /**
     * {@inheritdoc}
     * @return string[]
     */
    public function getServiceNames()
    {
        return array_keys($this->services);
    }

    /**
     * {@inheritdoc}
     * @param EntityInterface $entity_type
     */
    public function registerEntityType(EntityInterface $entity_type)
    {
        $name = $entity_type->getName();

        if (isset($this->entity_types[$name])) {
            throw new RuntimeException(sprintf('Can not override already registered entity type: %s', $name));
        }

        $this->entity_types[$name] = $entity_type;
    }

    /**
     * {@inheritdoc}
     * @param ExtensionInterface $extension
     */
    public function registerExtension(ExtensionInterface $extension)
    {
        $name = $extension->getName();

        if (isset($this->extensions[$name])) {
            throw new RuntimeException(sprintf('Can not override already registered extension: %s', $name));
        }

        $this->extensions[$name] = $extension;

        foreach ($extension->getEntityTypes() as $entity_type) {
            $this->registerEntityType($entity_type);
        }

        foreach ($extension->getServices() as $service) {
            $this->registerService($service);
        }

        foreach ($extension->getEntityQueries() as $query) {
            $this->registerEntityQuery($query);
        }
    }

    /**
     * {@inheritdoc}
     * @param ServiceInterface $service
     */
    public function registerService(ServiceInterface $service)
    {
        $name = $service->getName();

        if (isset($this->services[$name])) {
            throw new RuntimeException(sprintf('Can not override already registered service: %s', $name));
        }

        $this->services[$name] = $service;
    }

    public function registerEntityQuery(EntityQueryBuilderInterface $query)
    {
        $entity_type = $query->getEntityTypeName();

        if (isset($this->entity_queries[$entity_type])) {
            throw new RuntimeException(sprintf('Can not override already registered entity query: %s', $entity_type));
        }

        $this->entity_queries[$entity_type] = $query;
    }

    /**
     * {@inheritdoc}
     * @return EntityQueryBuilderInterface[]
     */
    public function getEntityQueries()
    {
        return $this->entity_queries;
    }

    /**
     * {@inheritdoc}
     * @return string[]
     */
    public function getEntityQueryNames()
    {
        return array_keys($this->entity_queries);
    }

    /**
     * {@inheritdoc}
     * @param string $name
     */
    public function getEntityQuery($name)
    {
        if (!isset($this->entity_queries[$name])) {
            throw new OutOfBoundsException(sprintf('Entity type is not registered: %s', $name));
        }

        return $this->entity_queries[$name];
    }

    /**
     * {@inheritdoc}
     * @return string[]
     */
    public function getEntityTypeNames()
    {
        return array_keys($this->entity_types);
    }

    /**
     * {@inheritdoc}
     * @param \GoCatalyze\SyncCenter\Entity\EntityInterface $entity
     * @param string $remote_entity_type
     */
    public function entityToArray(EntityInterface $entity, $remote_entity_type)
    {
        return $entity->getAttributes();
    }

    /**
     * {@inheritdoc}
     * @param array $input                 Input array
     * @param string $entity_type          System entity type, e.g. `drupal.entity`, …
     * @param string $remote_entity_type   Remote entiyt type, e.g. `node`, `user`, …
     */
    public function entityFromArray(array $input, $entity_type, $remote_entity_type)
    {
        $entity = $this->getEntityType($entity_type);

        foreach ($input as $attr_name => $attr_value) {
            $entity->setAttributeValue($attr_name, $attr_value);
        }

        return $entity;
    }

}
