<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures\Entity\Query;

use GoCatalyze\SyncCenter\Entity\Query\BaseEntityQuery;

class DemoEntityQuery extends BaseEntityQuery
{

    protected $entity_query_name = 'demo.entity_query';
    protected $query_field_updated_at = 'changed';
    protected $query_field_entity_type = 'entity_type';

    public function getQueryFieldUpdatedAt($remote_entity_type)
    {
        return 'changed';
    }

}
