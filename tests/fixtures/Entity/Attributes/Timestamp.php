<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures\Entity\Attributes;

use GoCatalyze\SyncCenter\Entity\EntityAttributeValidatorInterface;

class Timestamp implements EntityAttributeValidatorInterface
{

    public function validate($input)
    {
        return is_int($input) && $input >= 0;
    }

}
