<?php

namespace GoCatalyze\SyncCenter\Testing\Entity\Query;

use DateTime;
use GoCatalyze\SyncCenter\Testing\Fixtures\Entity\Query\DemoEntityQuery;
use GoCatalyze\SyncCenter\Testing\InterfacesTestCase;

/**
 * @group entityQuery
 */
class BaseEntityQueryTest extends InterfacesTestCase
{

    public function getQuery()
    {
        return new DemoEntityQuery();
    }

    public function testGetName()
    {
        $this->assertEquals('demo.entity_query', $this->getQuery()->getEntityTypeName());
    }

    public function testFields()
    {
        $query = $this->getQuery();
        $query->fields($fields = ['nid', 'title', 'published', 'create', 'updated']);
        $this->assertEquals($fields, $query->getFields());
    }

    public function testSort()
    {
        $query = $this->getQuery();
        $query->sort('pinned', 'DESC');
        $query->sort('created', 'DESC');
        $this->assertEquals(['pinned' => 'DESC', 'created' => 'DESC'], $query->getSorts());
    }

    public function testCondition()
    {
        $timestamp = strtotime('- 1 day');

        $query = $this->getQuery();
        $query->condition('nid', 1);
        $query->condition('created', $timestamp, '>=');
        $query->condition('published', false, '<>');

        $conditions = $query->getConditionGroups()['default']['expressions'];
        $this->assertEquals(['nid', 1, '='], $conditions[0]);
        $this->assertEquals(['created', $timestamp, '>='], $conditions[1]);
        $this->assertEquals(['published', false, '<>'], $conditions[2]);
    }

    public function testQueryLatestEntities()
    {
        $datetime = new DateTime('- 10 days');

        $query = $this->getQuery();
        $query->queryLatestEntities($datetime, 'node');

        $this->assertEquals('ASC', $query->getSorts()['changed']);
        $this->assertEquals(['changed', $datetime->format(DATE_ISO8601), '>'], $query->getConditionGroups()['default']['expressions'][0]);
    }

}
