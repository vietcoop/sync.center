<?php

/**
 * Check 'Install' section in README.md to know how to configure Drupal.
 */
use GoCatalyze\Drupal\DrupalEntity;
use GoCatalyze\Drupal\DrupalEntityQuery;
use GoCatalyze\Drupal\DrupalExtension;
use GoCatalyze\Drupal\DrupalService;
use GoCatalyze\SyncCenter\BaseManager;

/* @var $manager BaseManager */
/* @var $service DrupalService */
$manager = require_once __DIR__ . '/bootstrap.php';
$manager->registerExtension(new DrupalExtension());
$service = $manager->getService('Drupal', [
    'host' => 'http://go:go@services.dev.go1.com.vn/entity',
    'name' => 'tien',
    'pass' => 'tien'
    ]);

/**
 * Get Drupal's entity structure.
 */
print_r([
    'node › id Key' => $service->getRemoteEntityInfo('node')['entity keys']['id'],
    'node.article.body.label' => $service->getRemoteFieldInfoInstances()['node']['article']['body']['label']
]);

// ---------------------
// Query lastest nodes
// ---------------------
$latest_query = $manager->getEntityQuery('drupal.entity');
$latest_query->queryLatestEntities(strtotime('2014-01-01 00:00:00'), 'node');
print_r([
    'query string' => (string) $latest_query,
    'results' => $service->query($latest_query, 'node'),
]);

/**
 * Query for custom entity_type
 */
/* @var $query DrupalEntityQuery */
$query = $manager->getEntityQuery('drupal.entity');
$query->queryLatestEntities(new DateTime('2014-01-01 00:00:00'), 'views_user');
$query->setEntityType('views_user');
$query->setQueryFieldUpdatedAt('created');
print_r($service->query($query));

// ---------------------
// Query nodes with custom fields/filters/sort
// ---------------------
$custom_query = $manager->getEntityQuery('drupal.entity');
$custom_query->fields(['nid', 'uid', 'title', 'changed']);
$query->condition('status', 1);
$query->condition('type', 'article');
$query->sort('created', 'DESC');
print_r($service->query($query, 'node'));

// ---------------------
// Create new node.
// ---------------------
$entity = DrupalEntity::importFromArray([
        'entity_type' => 'node',
        'type' => 'article',
        'title' => 'Hello',
        'body' => ['und' => [['value' => 'Body']]
    ]]);

print_r($service->create($entity));

// ---------------------
// Update a node with on Drupal site has services_entity.module installed
//  In this example, we override default endpoint for node retrieving.
// ---------------------
$config = $service->getConfiguration();
$config['endpoints']['node']['GET'] = 'entity_node';
$service->setConfiguration($config);
$update_node = DrupalEntity::importFromArray([
        'nid' => 1,
        'title' => 'Demo node updated at ' . date(\DATE_RFC822),
        'body' => ['und' => [0 => ['value' => 'Demo body, edited at ' . date(\DATE_RFC822)]]],
    ]);
print_r($service->update($update_node, 'node'));

// ---------------------
// Update entity with reference attributes
// ---------------------
$ref_user = \GoCatalyze\Drupal\DrupalEntity::importFromArray([
        'uid' => 1,
        'init' => 'go_support.' . rand(100, 900) . '@x.v3k.net',
        'profile2[main]/field_full_name' => ['und' => [0 => ['value' => 'Jonhson English']]]
    ]);
print_r($service->update($ref_user, 'user'));

// ---------------------
// Find existence of an entity on remote Drupal
// ---------------------
/* @var $r_user DrupalEntity */
$u1 = DrupalEntity::importFromArray(['name' => 'tien']);
if ($_u1 = $service->loadFull($u1, 'user', ['name'])) {
    echo $_u1->getAttributeValue('name'); // tien
}

/* @var $_u2 DrupalEntity */
$u2 = DrupalEntity::importFromArray(['field_full_name' => 'Jonhson English']);
if ($_u2 = $service->loadFull($u2, 'profile2', ['field_full_name'])) {
    print_r($_u2);
}

$u3 = DrupalEntity::importFromArray([
        'name' => 'tien',
        'profile2[main]/field_full_name' => 'Jonhson English'
    ]);
if ($_u3 = $service->loadFull($u3, 'user', ['profile2[main]/field_full_name'])) {
    echo $_u3->getAttributeValue('name'); // tien
}
