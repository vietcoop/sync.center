<?php

namespace GoCatalyze\SyncCenter\Entity\Mapping;

class EntityMappingItem implements EntityMappingItemInterface
{

    private $source;
    private $destination;
    private $callback;

    public function __construct($source = null, $destination = null, $callback = null)
    {
        if (!is_null($source)) {
            $this->setSource($source);
        }

        if (!is_null($destination)) {
            $this->setDestination($destination);
        }

        if (!is_null($callback)) {
            $this->setCallback($callback);
        }
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @inheritdoc
     * @return null|callable
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @inheritdoc
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @inheritdoc
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @inheritdoc
     * @param callable $callable
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
    }

    /**
     * Create new item from array.
     *
     * @param array $array
     * @return EntityMappingItemInterface
     */
    public static function fromArray($array)
    {
        $f_source = $array['source'];
        $f_dest = $array['destination'];
        $f_callback = isset($array['callback']) ? $array['callback'] : null;
        return new static($f_source, $f_dest, $f_callback);
    }

}
