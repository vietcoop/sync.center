<?php

namespace GoCatalyze\SyncCenter\Entity\Mapping;

class EntityMapping implements EntityMappingInterface
{

    private $items;

    public function getMappingItems()
    {
        return $this->items;
    }

    public function addMappingItem(EntityMappingItemInterface $item)
    {
        $source = $item->getSource();
        $key = is_string($source) ? $source : md5(serialize($source));
        $this->items[$key] = $item;
    }

    public function getMappingItem($source)
    {
        return isset($this->items[$source]) ? $this->items[$source] : null;
    }

    /**
     * Create new object from array.
     *
     * @param array $array
     * @return EntityMappingInterface
     */
    public static function fromArray(array $array)
    {
        $me = new static();
        foreach ($array as $item) {
            $me->addMappingItem(EntityMappingItem::fromArray($item));
        }
        return $me;
    }

}
