<?php

namespace GoCatalyze\SyncCenter\Extensions\Salesforce;

use GoCatalyze\SyncCenter\Entity\BaseEntity;

/**
 * Generic entity of salesforce.
 */
class SalesforceEntity extends BaseEntity
{

    /** @var string */
    protected $entity_name = 'salesforce.entity';

}
