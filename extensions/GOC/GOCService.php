<?php

namespace GoCatalyze\SyncCenter\Extensions\GOC;

use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalService;
use GoCatalyze\SyncCenter\Extensions\GOC\Helper\EntityFullLoader;

class GOCService extends DrupalService
{

    /** @var $service_name Name of service */
    protected $service_name = 'goc';

    /** @var array */
    protected $config_schema = [
        'host'     => ['label' => 'Host name', 'type' => 'url', 'required' => true],
        'instance' => ['label' => 'Instance', 'type' => 'string', 'required' => true],
        'name'     => ['label' => 'Username', 'type' => 'string', 'required' => true],
        'pass'     => ['label' => 'Password', 'type' => 'string', 'required' => true],
    ];

    /**
     * {@inheritdoc}
     */
    protected function getDefaultClient()
    {
        $config = $this->getConfiguration();
        $client = new GOCClient();
        $client->setBaseUrl($config['host']);
        $client->setConfig($config);
        return $client;
    }

    /**
     * {@inheritdoc}
     * @param string $type
     * @return string
     */
    public function getRemoteEntityIdKey($type)
    {
        return $this->getRemoteEntityInfo($type)['key'];
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    protected function getAllRemoteEntityInfo($refresh = false)
    {
        if (!empty($this->remote_entity_info) && !empty($this->config['entity_info'])) {
            $this->remote_entity_info = $this->config['entity_info'];
            unset($this->config['entity_info']);
        }

        if ($refresh || empty($this->remote_entity_info)) {
            $this->remote_entity_info = [];
            foreach ($this->getClient()->createRequest('GET', 'entity/details.json')->send()->json() as $_info) {
                $entity_type = "{$_info['type']}.{$_info['bundle']}";
                $this->remote_entity_info[$entity_type] = [
                    'entity_type' => $entity_type,
                    'label'       => $_info['title']] + $_info;
            }
        }

        return $this->remote_entity_info;
    }

    public function getEntityFullLoader()
    {
        if (null === $this->remote_entity_loader) {
            $this->setEntityFullLoader(new EntityFullLoader($this));
        }
        return $this->remote_entity_loader;
    }

    protected function isDrupalFieldName($attr_name, $type)
    {
        foreach ($this->getRemoteEntityInfo($type)['fields'] as $f_info) {
            if ($attr_name === $f_info['name']) {
                return true;
            }
        }
        return false;
    }

}
