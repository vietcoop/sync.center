<?php

namespace GoCatalyze\SyncCenter;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Query\EntityQueryBuilderInterface;
use Guzzle\Http\ClientInterface as GuzzleClientInterface;

interface ClientInterface extends GuzzleClientInterface
{

    /**
     * Query for remote entities.
     *
     * @param EntityQueryBuilderInterface $query
     * @param string $remote_entity_type
     * @return EntityInterface[]
     */
    public function doQuery(EntityQueryBuilderInterface $query, $remote_entity_type);

    /**
     * Create new entity on remote service.
     *
     * @param EntityInterface $entity
     * @param string $remote_entity_type
     * @return array Associated array of result, examples:
     *      ['status' => 'OK', 'id' => 123]
     *      ['status' => 'FAILED', 'message' => 'Error message']
     */
    public function doCreate(EntityInterface $entity, $remote_entity_type);
}
