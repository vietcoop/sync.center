<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Salesforce;

use GoCatalyze\SyncCenter\Entity\Mapping\EntityMapping;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceEntity;
use PHPUnit_Framework_TestCase;

abstract class SalesforceTestCase extends PHPUnit_Framework_TestCase
{
    /**
     * @return SalesforceEntity
     */
    protected function dummyFullEntiytContact()
    {
        $entity = $this->dummyEntityContact();
        $entity->setAttributeValue('Id', 112233);
        return $entity;
    }

    /**
     * @return SalesforceEntity
     */
    protected function dummyEntityContact()
    {
        $entity = new SalesforceEntity();
        $entity->setAttributeValues([
            'Name' => 'John English',
            'Mail' => 'john@english.com',
            'drupal_id__c' => 222,
        ]);

        return $entity;
    }

    protected function dummyUniques()
    {
        return ['drupal_id__c'];
    }

    protected function dummyMapping($include_ref)
    {
        $items = [
            ['source' => 'uid', 'destination' => 'drupal_id__c'],
            ['source' => 'name', 'destination' => 'Name'],
            ['source' => 'mail', 'destination' => 'Mail']
        ];

        if ($include_ref) {
            $items = array_merge($items, [
                ['source' => 'author_name', 'destination' => 'user[default]/name'],
                ['source' => 'author_mail', 'destination' => 'user[default]/mail']
            ]);
        }

        return EntityMapping::fromArray($items);
    }

}
