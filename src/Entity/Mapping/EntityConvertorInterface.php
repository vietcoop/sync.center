<?php

namespace GoCatalyze\SyncCenter\Entity\Mapping;

use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\ServiceInterface;

/**
 * Entity convert uses mapping-definition to convert entity from structure of
 * source-instance to destination-instance. Sub-tasks:
 *
 * - Convert fields:
 *      1. field -> field mapping
 *      2. [first_name, last_name] -> full_name
 *      3. ReferenceObjectField -> ReferenceObjectField
 * - Use destination.unique-fields to find destination entity ID.
 * - Load missing fields in destination-entity.
 */
interface EntityConvertorInterface
{

    /**
     * Cast entity to other type using a mapping array.
     *
     * @param EntityInterface $entity
     * @param string $dest_entity
     * @param EntityMappingInterface $mapping
     * @return EntityInterface
     */
    public function convert(EntityInterface $entity, $dest_entity, EntityMappingInterface $mapping);

    /**
     * @return \GoCatalyze\SyncCenter\ManagerInterface
     */
    public function getManager();

    /**
     * Inject source service.
     *
     * @param ServiceInterface $service
     * @return EntityConvertorInterface
     */
    public function setSourceService(ServiceInterface $service);

    /**
     * @return ServiceInterface
     */
    public function getSourceService();

    /**
     * Inject destination service.
     *
     * @param ServiceInterface $service
     * @return EntityConvertorInterface
     */
    public function setDestinationService(ServiceInterface $service);

    /**
     * @return ServiceInterface
     */
    public function getDestinationService();
}
