<?php

namespace GoCatalyze\SyncCenter\Testing\Fixtures;

use GoCatalyze\SyncCenter\BaseManager;

class DemoManager extends BaseManager
{

    public function __construct()
    {
        $this->registerExtension(new DemoExtension());
    }

}
