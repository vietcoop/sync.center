<?php

namespace GoCatalyze\SyncCenter\Extensions\Salesforce;

use GoCatalyze\SyncCenter\BaseService;
use GoCatalyze\SyncCenter\Entity\EntityInterface;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMappingInterface;
use GoCatalyze\SyncCenter\Extensions\Salesforce\SalesforceClient;

/**
 * Salesforce.
 *
 * @package GoCatalyze\Service
 */
class SalesforceService extends BaseService
{

    /** @var string */
    protected $service_name = 'salesforce';

    /** @var array */
    protected $config_schema = [
        'client_id'     => ['label' => 'Client ID', 'type' => 'url', 'required' => true],
        'client_secret' => ['label' => 'Client secret', 'type' => 'string', 'required' => true],
        'instance_url'  => ['label' => 'Instance URL', 'type' => 'string', 'required' => true],
        'refresh_token' => ['label' => 'Refresh Token', 'type' => 'string', 'required' => true],
        'username'      => ['label' => 'User name', 'type' => 'string', 'required' => true],
        'password'      => ['label' => 'Password', 'type' => 'string', 'required' => true],
    ];

    /**
     * {@inheritdoc}
     * @return SalesforceClient
     */
    public function getClient()
    {
        if (null === $this->client) {
            $client = new SalesforceClient();
            $client->setConfig($this->config);
            $this->setClient($client);
        }
        return $this->client;
    }

    /**
     * {@inheritdoc}
     * @param string $type
     * @return string
     */
    public function getRemoteEntityIdKey($type)
    {
        return 'Id';
    }

    /**
     * {@inheritdoc}
     * @return array
     */
    public function getAllRemoteEntityInfo($refresh = false)
    {
        static $entityInfo = [];

        if ($refresh || empty($entityInfo)) {
            $client = $this->getClient()->getRealClient();
            foreach ($client->getObjectTypes() as $objectType) {
                $entityInfo[$objectType] = $client->getObjectTypeInfo($objectType);
                $entityInfo[$objectType]['label'] = $entityInfo[$objectType]['entity_type'] = $objectType;
            }
        }

        return $entityInfo;
    }

    /**
     * {@inheritdoc}
     * @param string $type
     * @param bool $refresh
     * @return array
     */
    public function getRemoteEntityInfo($type = null, $refresh = false)
    {
        $entityInfo = $this->getAllRemoteEntityInfo($refresh);
        if (null === $type) {
            return $entityInfo;
        }
        return isset($entityInfo[$type]) ? $entityInfo[$type] : null;
    }

    public function loadFull(EntityInterface $entity, $type, EntityMappingInterface $mapping, array $uniques = [])
    {
        $side = empty($uniques) ? 'source' : 'destination';

        if ('source' === $side) {
            // no need loading extra infor for now
            return $entity;
        }

        $query = new SalesforceEntityQuery();
        $query->setRemoteEntityType($type);
        $query->fields(["Id"]);
        foreach ($uniques as $unique_field) {
            // case of reference field: Object.Contact__c = string
            if ('Contact__c' === $unique_field) {
                $field_info = $this->getConfiguration()['entity_info'][$type]['fields'][$unique_field];
                if (!empty($field_info['referenceTo'][0])) {
                    $query->condition(str_replace('__c', '__r.name', $unique_field), "'{$entity[$unique_field]}'");
                }
            }
            else {
                $query->condition($unique_field, $entity[$unique_field]);
            }
        }

        if ($return = $this->getClient()->doQuery($query, $type)) {
            return reset($return);
        }
    }

    /**
     * {@inheritdoc}
     * @param EntityInterface $entity
     * @param string $type
     * @param EntityMappingInterface $mapping
     * @param string[] $uniques
     */
    public function merge(EntityInterface $entity, $type, EntityMappingInterface $mapping, array $uniques)
    {
        if ($remote_entity = $this->loadFull($entity, $type, $mapping, $uniques)) {
            if (isset($remote_entity)) {
                $id = $remote_entity['Id'];
                $entity['Id'] = $id;
                return $this->getClient()->doUpdate($entity, $id, $type);
            }
        }

        return $this->getClient()->doCreate($entity, $type);
    }

}
