<?php

namespace GoCatalyze\SyncCenter\Extensions\Testing\Drupal;

use GoCatalyze\SyncCenter\BaseManager;
use GoCatalyze\SyncCenter\Entity\Mapping\EntityMapping;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalEntity;
use GoCatalyze\SyncCenter\Extensions\Drupal\DrupalExtension;
use PHPUnit_Framework_TestCase;

abstract class DrupalTestCase extends PHPUnit_Framework_TestCase
{

    /** @var BaseManager */
    protected $manager;

    public function getManager()
    {
        if (null === $this->manager) {
            $this->manager = new BaseManager();
            $this->manager->registerExtension(new DrupalExtension());
        }
        return $this->manager;
    }

    protected function getDummyEntity($include_ref = false)
    {
        $entity = new DrupalEntity();
        $entity->setAttributeValues([
            'nid' => 123,
            'type' => 'article',
            'title' => 'Demo article node',
            'body' => 'Dummy body text…',
            'path' => 'article/demo-article-node.html',
        ]);

        if ($include_ref) {
            $entity->setAttributeValues([
                'user[default]/name' => 'John English',
                'user[default]/mail' => 'john@english.com',
            ]);
        }

        return $entity;
    }

    protected function getDummyMapping($include_ref = false)
    {
        $items = [
            ['source' => 'drupal_id__c', 'destination' => 'nid'],
            ['source' => 'drupal_entity__c', 'destination' => 'type'],
            ['source' => 'title__c', 'destination' => 'title'],
            ['source' => 'body__c', 'destination' => 'body'],
            ['source' => 'slug__c', 'destination' => 'path'],
        ];

        if ($include_ref) {
            $items = array_merge($items, [
                ['source' => 'author_name', 'destination' => 'user[default]/name'],
                ['source' => 'author_mail', 'destination' => 'user[default]/mail']
            ]);
        }

        return EntityMapping::fromArray($items);
    }

    protected function getDummyUniques()
    {
        return ['path', 'title'];
    }

}
